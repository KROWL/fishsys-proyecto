grant select, update, insert on FishSysDBA.EspeciesAcuaticas to biologo; 
grant select, update, insert on FishSysDBA.CoordenadasGeograficas to biologo; 
grant select, update, insert on FishSysDBA.EspeciesPorHabitats to biologo; 
grant select, update, insert on FishSysDBA.Habitats to biologo; 
grant select, update, insert on FishSysDBA.Taxonomias to biologo; 
grant select, update, insert on FishSysDBA.ZonasGeos to biologo; 

grant biologo to Eduard; 

revoke select on table from privilegio