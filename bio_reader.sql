grant select on table FishSysDBA.EspeciesAcuaticas to biologo_read; 
grant select on table FishSysDBA.CoordenadasGeograficas to biologo_read; 
grant select on table FishSysDBA.EspeciesPorHabitats to biologo_read; 
grant select on table FishSysDBA.Habitats to biologo_read; 
grant select on table FishSysDBA.Taxonomias to biologo_read; 
grant select on table FishSysDBA.ZonasGeos to biologo_read;

