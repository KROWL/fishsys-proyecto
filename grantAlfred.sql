grant select, update, insert on table FishSysDBA.EspeciesAcuaticas to biologo; 
grant select, update, insert on table FishSysDBA.CoordenadasGeograficas to biologo; 
grant select, update, insert on table FishSysDBA.EspeciesPorHabitats to biologo; 
grant select, update, insert on table FishSysDBA.Habitats to biologo; 
grant select, update, insert on table FishSysDBA.Taxonomias to biologo; 
grant select, update, insert on table FishSysDBA.ZonasGeos to biologo; 

grant biologo to alfred; 