/**
   @author: Kirlian Ortiz 
   @description: Prueba Proyecto v.01
**/

import java.util.Scanner; 

class EspecieAcuatica{
   
   private int idEspecie; 
   private String nombreEspecie; 
   private String reino; 
   private String filo; 
   private String clase; 
   private String orden; 
   private String familia; 
   private String genero; 
   private String tamano; 
   
   public EspecieAcuatica(){
      idEspecie     = 001;
	  nombreEspecie = "Bagre";
	  reino         = "animalia";
	  filo          = "animalia";
	  clase         = "animalia";
	  orden         = "animalia";
	  familia       = "animal";
	  genero        = "animal";
	  tamano        = "2 a 3 mts de largo";
   }
   
   public EspecieAcuatica(int idEspecie, String nombreEspecie, String reino, String filo, String clase, String orden, String familia, String genero, String tamano){
      
	  this.idEspecie = idEspecie;
	  this.nombreEspecie = nombreEspecie;
	  this.reino = reino;
	  this.filo = filo;
	  this.clase = clase;
	  this.orden = orden;
	  this.familia = familia;
	  this.genero = genero;
	  this.tamano = tamano;
	  
   }
   
   //public  getnombre(){}
   public void mostrarDatos(){
      System.out.println("\n\t idEspecie            | "+idEspecie+
	                     "\n\t nombreEspecie        | "+nombreEspecie+
						 "\n\t reino                | "+reino+
						 "\n\t filo                 | "+filo+
						 "\n\t clase                | "+clase+
						 "\n\t orden                | "+orden+
						 "\n\t familia              | "+familia+
						 "\n\t genero               | "+genero+
						 "\n\t tamano               | "+tamano);
   }
   
}

class Habitat{
   
   private int idHabitat; 
   private String nombre; 
   private String temperatura; 
   private EspecieAcuatica especie;
   private ZonaGeo zG;
   
   public Habitat(){
      idHabitat = 001;
	  nombre    = "Canada";
	  temperatura = "12 grados celsius";
	  
   }
   
   public Habitat (int idHabitat, String nombre, String temperatura){
	  
	  this.idHabitat = idHabitat;
	  this.nombre    = nombre;
	  this.temperatura = temperatura;
	  
   }
   
   //agregacion de una especie al habitat 
   public void setEspecieAcuatica(EspecieAcuatica esp0){
      this.especie = esp0;
   }
   
   public EspecieAcuatica getEspecieAcuatica(){
     return especie;
   }
   
   public void setZonaGeo(ZonaGeo zona){
      this.zG = zona;
   }
   
   public ZonaGeo getZonaGeo(){
      return zG;
   }
   
   public void mostrarEspecie(){
      System.out.println("\n\t idHabitat            | "+idHabitat+
	                     "\n\t nombre habitat       | "+nombre+
						 "\n\t temperatura          | "+temperatura);
						 //return especie; se puede poner aca tambien 
   }
   
   public String getnombreHabitat(String nombre){
      return nombre = nombre;
   } 
   
}

class ZonaGeo{
   
   private int idZonaGeo;
   private String continente;
   private String tipoMasaAcuatica;
   private String nombreMasaAcuatica;
   private CoordenadaGeografica coordGeografica = new CoordenadaGeografica();
   
   public ZonaGeo(){
      idZonaGeo = 001;
	  continente = "Americano";
	  tipoMasaAcuatica = "Lago";
	  nombreMasaAcuatica = "Lake";
   }
   
   /* public void setCoordGeo(CoordenadaGeografica coorGeo){
      this.coordGeografica = coorGeo;
   }
   
   public CoordenadaGeografica getCoordGeo(){
      return coordGeografica;
   } */
   
   public void mostrarZonaGeo(){
      System.out.println("\n\t idZonaGeo            | "+idZonaGeo+
	                     "\n\t Continente           | "+continente+
						 "\n\t Tipo Masa Acuatica   | "+tipoMasaAcuatica+
						 "\n\t Nombre Masa Acuatica | "+nombreMasaAcuatica);
						 coordGeografica.mostrarCoordGeo();
   }
   
   
}

class CoordenadaGeografica{
   
   private int idCoordGeo; 
   private String latitud;
   private String longitud;
   
   public CoordenadaGeografica(){
      idCoordGeo = 001;
	  latitud    = "44.037239";
	  longitud   = "-79.267650";
   }
   
   public CoordenadaGeografica(int idCoordGeo, String lat, String lon){
      
	  this.idCoordGeo = idCoordGeo;
	  this.latitud    = lat;
	  this.longitud   = lon;
   }
   
   public void mostrarCoordGeo(){
      System.out.println("\n\t idCoordGeo           | "+idCoordGeo+
	                     "\n\t latitud              | "+latitud+
						 "\n\t longitud             | "+longitud);
	  
   }
   
}
class Taxonomia{
   private EspecieAcuatica esp02;
   private Habitat habitat02;
   
   public Taxonomia(){
      
   }
   
   public EspecieAcuatica agregarEspecie(EspecieAcuatica esp){
      this.esp02 = esp;
	  return esp02;
   }
   
   public Habitat agregarHabitat(Habitat habitat){
      this.habitat02 = habitat;
	  return habitat02;
   }
   
}
class ControlTaxonomia{
   
   private Taxonomia t01;
   
   ControlTaxonomia(){
      
   }
   
   public void agragarTaxonomia(Taxonomia taxonomia){
      this.t01 = taxonomia;
   }
   
   public Taxonomia mostrarTaxonomia(){
      return t01;
   }
   
}
public class Proyecto01{
   
   public static void main(String arg[]){
      
	  Habitat habitat   = new Habitat(001, "Alaska", "10 grados celsius");
	  Habitat habitat01 = new Habitat();
	  
	  EspecieAcuatica esp01   = new EspecieAcuatica();
	  ZonaGeo         zonaGeo = new ZonaGeo();
	  
	  habitat.setEspecieAcuatica(esp01);
	  habitat.getEspecieAcuatica().mostrarDatos();
	  
	  habitat.setZonaGeo(zonaGeo); 
	  habitat.getZonaGeo().mostrarZonaGeo();
	  
	  habitat.mostrarEspecie();
	  habitat01.mostrarEspecie();
	  
	  System.out.println("\n\t Taxonomia \n");
	  Taxonomia t01 = new Taxonomia();
	  
	  t01.agregarEspecie(esp01).mostrarDatos();
	  t01.agregarHabitat(habitat).mostrarEspecie();
	  t01.agregarHabitat(habitat).getZonaGeo().mostrarZonaGeo();
	  
	  System.out.println("\n\t Control Taxonomia \n");
	  
	  ControlTaxonomia t02 = new ControlTaxonomia();
	  t02.agragarTaxonomia(t01);
	  t02.mostrarTaxonomia().agregarEspecie(esp01).mostrarDatos();
	  t02.mostrarTaxonomia().agregarHabitat(habitat).mostrarEspecie();
	  t02.mostrarTaxonomia().agregarHabitat(habitat).getZonaGeo().mostrarZonaGeo();
	  
   }
   
}