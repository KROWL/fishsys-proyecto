//package utility.configuration;





package vista;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import dominio.controller.Controller; 

public class AppConfiguration {
	
	public static void setLookAndFeel(){
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if (info.getName().contains("Nimbus")) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		  
		}
	}
}
