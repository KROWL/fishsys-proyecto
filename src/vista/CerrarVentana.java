
/**
   
   @author Kirlian Ortiz 
   @version 0 
   @descripcion Clase Window Close 
   
**/ 


package vista; 

import javax.swing.JOptionPane; 
import java.awt.event.WindowEvent; 
import java.awt.event.WindowAdapter; 
 
import dominio.controller.Controller; 
import vista.LoginVista;

public class CerrarVentana extends WindowAdapter{
   
   private Vista vista = null; 
   
   public CerrarVentana(Vista vista){
	   this.vista = vista; 
   }
   public CerrarVentana(){
	  
   }
   @Override  
   public void windowClosing(WindowEvent e){
      
	  int dialogWindow = JOptionPane.showConfirmDialog(null, "Seguro de cerrar el programa? (GUARDE LOS DATOS ANTES DE CERRAR )"
	                                                 , "ADVICE", JOptionPane.YES_NO_OPTION);  
	     
	     if(dialogWindow == JOptionPane.YES_OPTION){
		    
			Controller.close(); 
			vista.dispose(); 
			
			new LoginVista();
			//System.exit(1); 
			//cierra la aplicaion por completo 
			
		 }
		
	  
   }

   
}