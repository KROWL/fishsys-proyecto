/**
   
   @author Kirlian Ortiz 
   @version 0 
   @descripcion Ventana general 
   
**/ 


package vista; 

import javax.swing.JFrame; 
import javax.swing.ImageIcon; 
import java.awt.Image; 
import java.awt.Dimension; 
import java.awt.Toolkit; 
import java.awt.event.ActionListener;  
import javax.swing.JLabel; 
import javax.swing.JPanel; 
import java.awt.Color; 
import java.awt.Graphics; 
import javax.swing.JTable; 
import javax.swing.table.*;  
import java.awt.BorderLayout; 
import javax.swing.BorderFactory; 


//import javax.swing.WindowConstants; 

public abstract class VentanaUtilidades extends JFrame implements ActionListener{
   
   public void VentanaUtilidades(){} 
   
   public void crearVentana(String title){
      
	  setTitle(title + " FishSys v1.4 "); 
	  //setSize(400, 500); 
	  setResizable(true); 
	  setLocationRelativeTo(null); 
	  setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); 
	  addWindowListener(new CloseWindow()); 
	  
   }
   
   //add opciones a los elementos 
   public abstract void addOpciones(); 
   
   //add los elements al panel 
   public abstract void addElementos(); 
   
   
   //public abstract void mostrarTablas(); 
   //cambiar icon 
   //public abstract void cambiarIcono(); 
   
   public void imgFondo(){
	   
	   JPanel pnlTop = new JPanel() {  //Imagen de Fondo

    public void paintComponent(Graphics g) {  
          Image img = Toolkit.getDefaultToolkit().getImage(  
          VentanaUtilidades.class.getResource("img/pezTrop.jpg"));  
          g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);  
        }  
	}; 
	
    //ImageIcon img = new ImageIcon("img/img2.png"); 
		//JLabel lbl = new JLabel(img); 
		//lbl.setBounds(200,10,500,300);
    //pnlTop.add(lbl); 

    pnlTop.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(173, 173, 166)));
    pnlTop.setPreferredSize(new Dimension(200, 200));
    add(pnlTop, BorderLayout.PAGE_START);
	   
		 
   }
   
	public void cambiarIcono(){
		//maenjo de excepcion para conocer caracterisicas de la imagen 
		try{
			
			Image icono = Toolkit.getDefaultToolkit().getImage(getClass().getResource("img/img01.jpg"));
			
		    this.setIconImage(icono);
		}catch(Exception e){
			
		}
		
		
	}
   //add evento 
   
   
}