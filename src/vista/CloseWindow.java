
/**
   
   @author Kirlian Ortiz 
   @version 0 
   @descripcion Clase Window Close 
   
**/ 


package vista; 

import javax.swing.JOptionPane; 
import java.awt.event.WindowEvent; 
import java.awt.event.WindowAdapter; 
 
import dominio.controller.Controller; 


public class CloseWindow extends WindowAdapter{
   
   @Override  
   public void windowClosing(WindowEvent e){
      
	  int dialogWindow = JOptionPane.showConfirmDialog(null, "Seguro de cerrar el programa?"
	                                                   , "ADVICE", JOptionPane.YES_NO_OPTION);  
	     
	     if(dialogWindow == JOptionPane.YES_OPTION){
		    
			Controller.close(); 
			System.exit(-1); //cierra la aplicaion por completo 
		 }
		
	  
   }

   
}