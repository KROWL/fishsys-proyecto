/**
   
   @author Kirlian Ortiz 
   @descripcion vistaUtilities 
   
**/ 
package vista; 

import javax.swing.JFrame; 
import javax.swing.ImageIcon; 
import java.awt.Image;  
import java.awt.Dimension; 
import java.awt.Toolkit; 
import java.awt.event.ActionListener; 
import java.awt.event.ActionEvent;  
import javax.swing.JLabel; 
import javax.swing.JPanel; 
import java.awt.Color; 
import java.awt.Graphics; 
import javax.swing.JTable; 
import javax.swing.table.*;  
import java.awt.BorderLayout; 
import javax.swing.BorderFactory; 
import javax.swing.JPanel; 
import javax.swing.JTextField; 
import javax.swing.JButton; 
import java.awt.event.MouseAdapter; 
import java.awt.event.MouseEvent; 
import java.awt.Cursor; 
import java.awt.Font;
import java.awt.Graphics2D; 
import java.awt.GradientPaint; 
import java.awt.RenderingHints; 
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.io.*;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.UIManager;
import javax.swing.JOptionPane;
import javax.swing.Icon; 
import javax.swing.JScrollPane;
import java.lang.Object; 
import javax.swing.table.DefaultTableModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener; 
import javax.swing.event.ListSelectionEvent; 
import java.util.ArrayList; 
import javax.swing.JToolTip;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.JasperPrint;

import java.util.HashMap;
import java.util.Map;
import dominio.controller.Controller;
import dominio.EspeciesAcuaticas; 


public class VistaMostrar extends JPanel implements ITable{
	
	private JButton btnInsert = null; 
	private JButton btnModificar = null; 
	private JButton btnConsult = null; 
	
	private static JButton btnLog = null; 
	private static JButton btnReporte = null; 
	private static JButton btnReport = null; 
	private JLabel labelIMG = null;	
	private JButton btnClose = null; 
	private Icon iconIMG = null; 
	private ImageIcon imagen = null; 
	private Image img = null; 
	private JLabel lblBuscar = null; 
	private static Vista vista =null;
	private static JTable tabla = null; 
	private JTable tablaLogs = null; 
	private JScrollPane scrollLogs = null; 
	private JScrollPane scroll = null; 
	private JTextField txtBuscar = null; 
	private static VistaMostrar vistaMostrar = null; 
	
	private JPanel panelContent = null; 
	private VistaLog vistaLog; 
	private JPanel panel = null; 
	public static String LOGS = "log"; 
	private CardLayout panelCard = null; 
	private static String valor;
	private ViewGuardar vistaGuardar=null;
	private Controller controller = null; 
	
	
	
	public VistaMostrar(Vista vista){
		
		this.setLayout(new BorderLayout()); 
		this.vista = vista;
		
		valor ="";
		addVentana(); 
		addElement(); 
		//cambiarColorFondo(); 
		evenClick(); 
		imgFondo(); 
		
		JPanel panel = new JPanel(); 
		getVisibleLog(true); 
	    this.setBorder(BorderFactory.createTitledBorder("Mostrar datos")); 
	    //this.add(new JLabel("Label")); 22,145,217
	
		this.setBackground(new Color(77,176,230));
		
	} 
	
	public VistaMostrar(){
		
	}
	public void addVentana(){
		
		
		ImageIcon imgSave = new ImageIcon(getClass().getResource("img/search24x24_01.png")); 
		ImageIcon imgUpdate = new ImageIcon(getClass().getResource("img/update0124x24.png")); 
		//ImageIcon imgSearch = new ImageIcon("img/grid_32x32.png"); 
		ImageIcon imgReporte = new ImageIcon(getClass().getResource("img/reporte0.png"));
		ImageIcon imgClose = new ImageIcon(getClass().getResource("img/close16x16.png")); 
		ImageIcon imgLogs = new ImageIcon(getClass().getResource("img/bitacora24x24.png")); 
		//getClass.getResource()
		lblBuscar = new JLabel("Buscar Nombre Comun"); 
		txtBuscar = new JTextField(); 
		
		btnInsert = new JButton("Buscar", imgSave);
		btnModificar = new JButton("Refresh", imgUpdate);
		//btnConsult = new JButton("Mostrar", imgSearch); 
		btnClose = new JButton("cerrar session", imgClose); 
		btnLog = new JButton("Logs", imgLogs); 
		btnReport = new JButton("Reporte",imgReporte);
		
		panelCard = new CardLayout(); 
		panel = new JPanel(panelCard); 
		//vistaLog = new VistaLog(this);
		
		txtBuscar.setToolTipText("buscar nombre de comun de especie");
		btnInsert.setToolTipText("buscar");
		btnModificar.setToolTipText("refrescar datos de la tabla");
		btnLog.setToolTipText("actividades de lo usuario"); 
		btnReport.setToolTipText("generar reporte");
		
		
		controller = new Controller(); 
		
		lblBuscar.setBounds(20, 70, 150, 25); 
		txtBuscar.setBounds(150, 70, 200, 25); 
		
		btnInsert.setBounds(100, 400, 170, 35); 
		btnModificar.setBounds(310, 400, 170,35); 
		//btnConsult.setBounds(510, 400, 170, 35); 
		btnClose.setBounds(600, 20, 120, 25); 
		btnLog.setBounds(510, 400, 170,35); 
		btnClose.setBounds(600,20,120,26);
		btnReport.setBounds(600,50,150,50); 
		
		tabla = new JTable(){
		    @Override
		       public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		           Component component = super.prepareRenderer(renderer, row, column);
		           int rendererWidth = component.getPreferredSize().width;
		           TableColumn tableColumn = getColumnModel().getColumn(column);
		           tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
		           return component;
		        }
		};
		
		
		tabla.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent lse) {
		        if (!lse.getValueIsAdjusting()) {
					for(int i =0;i<tabla.getModel().getColumnCount();i++){
						if(tabla.getSelectedRowCount()>0){
							EspeciesAcuaticas especieAcuatica = new EspeciesAcuaticas();
								especieAcuatica.setId((int)tabla.getModel().getValueAt(tabla.getSelectedRow(),0)); 
								especieAcuatica.setNombreComun  ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),1));
								especieAcuatica.setReino        ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),2));
								especieAcuatica.setFilo         ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),3));
								especieAcuatica.setClase        ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),4));
								especieAcuatica.setOrden        ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),5));
								especieAcuatica.setFamilia      ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),6));
								especieAcuatica.setGenero       ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),7));
								especieAcuatica.setTamano       ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),8));
								especieAcuatica.setContinente   ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),9));
								especieAcuatica.setMar          ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),10));
								especieAcuatica.setOceano       ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),11)); 
								especieAcuatica.setLongitud     ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),12));
								especieAcuatica.setLatitud      ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),13));
								especieAcuatica.setNombreHabitat((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),14));
								especieAcuatica.setProfundidad  ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),15));
								especieAcuatica.setTemperatura  ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),16)); 
								vista.setUpdateInfo(especieAcuatica);
								vista.changePanel(Vista.UPDATE);
		                   
							}
						}
						
					}
		    }
		}); 
		
		controller.getEspeciesAcuaticasTModel(this);
		tabla.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		scroll = new JScrollPane(tabla); 
		scroll.setHorizontalScrollBarPolicy(javax.swing.JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS); 
		scroll.setVerticalScrollBarPolicy(javax.swing.JScrollPane.VERTICAL_SCROLLBAR_ALWAYS); 
		scroll.setViewportView(tabla); 
		
		//tabla.setAutoResizeModel(1); 
		tabla.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); 
		tabla.getColumnModel().getColumn(0).setPreferredWidth(120);
		tabla.getColumnModel().getColumn(1).setPreferredWidth(50);
		tabla.getColumnModel().getColumn(2).setPreferredWidth(100);
		tabla.getColumnModel().getColumn(3).setPreferredWidth(90);
		tabla.getColumnModel().getColumn(4).setPreferredWidth(90);
		tabla.getColumnModel().getColumn(6).setPreferredWidth(120);
		tabla.getColumnModel().getColumn(7).setPreferredWidth(94);
		tabla.getColumnModel().getColumn(8).setPreferredWidth(95);
		tabla.getColumnModel().getColumn(9).setPreferredWidth(40);
		tabla.getColumnModel().getColumn(10).setPreferredWidth(27);
		tabla.getColumnModel().getColumn(11).setPreferredWidth(120);
		tabla.getColumnModel().getColumn(12).setPreferredWidth(100);
		tabla.getColumnModel().getColumn(13).setPreferredWidth(90);
		tabla.getColumnModel().getColumn(14).setPreferredWidth(90);
		tabla.getColumnModel().getColumn(15).setPreferredWidth(94);
		

		scroll.setBounds(70, 150, 600, 200); 
		btnInsert.setFont(new Font("Serif",Font.BOLD,20));
        btnInsert.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnInsert.setBorderPainted(false);
		btnInsert.addMouseListener(new java.awt.event.MouseAdapter() {
           public void mouseEntered(java.awt.event.MouseEvent evt) {
             btnInsert.setBackground(Color.decode("#4fc3f7"));
		   }
		   public void mouseExited(java.awt.event.MouseEvent evt) {
			 btnInsert.setBackground(UIManager.getColor("control"));
		   }
		});
		
		btnModificar.setFont(new Font("Serif",Font.BOLD,20));
		btnModificar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		//btnModificar.setForeground(Color.WHITE);
		btnModificar.setFocusPainted(true);
		btnModificar.setBorderPainted(false);
		btnModificar.addMouseListener(new java.awt.event.MouseAdapter() {
             public void mouseEntered(java.awt.event.MouseEvent evt) {
               btnModificar.setBackground(Color.decode("#4fc3f7"));
             }
       
             public void mouseExited(java.awt.event.MouseEvent evt) {
               btnModificar.setBackground(UIManager.getColor("control"));
             }
        });
		btnLog.setFont(new Font("Serif",Font.BOLD,20));
		btnLog.setCursor(new Cursor(Cursor.HAND_CURSOR));
		//btnModificar.setForeground(Color.WHITE);
		btnLog.setFocusPainted(true);
		btnLog.setBorderPainted(false);
		btnLog.addMouseListener(new java.awt.event.MouseAdapter() {
             public void mouseEntered(java.awt.event.MouseEvent evt) {
               btnLog.setBackground(Color.decode("#4fc3f7"));
             }
       
             public void mouseExited(java.awt.event.MouseEvent evt) {
               btnLog.setBackground(UIManager.getColor("control"));
             }
        });
		/**
		btnConsult.setFont(new Font("Serif",Font.BOLD,20));
		btnConsult.setCursor(new Cursor(Cursor.HAND_CURSOR));
		//btnConsult.setForeground(Color.WHITE);
		btnConsult.setFocusPainted(true);
		btnConsult.setBorderPainted(false);
		btnConsult.addMouseListener(new java.awt.event.MouseAdapter() {
             public void mouseEntered(java.awt.event.MouseEvent evt) {
               btnConsult.setBackground(Color.gray);
             }
       
             public void mouseExited(java.awt.event.MouseEvent evt) {
               btnConsult.setBackground(UIManager.getColor("control"));
             }
        }); */
		
		btnClose.setFont(new Font("Serif",Font.PLAIN,12));
		btnClose.setCursor(new Cursor(Cursor.HAND_CURSOR));
		//btnModificar.setForeground(Color.WHITE);
		btnClose.setFocusPainted(true);
		btnClose.setBorderPainted(false);
		btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
             public void mouseEntered(java.awt.event.MouseEvent evt) {
               btnClose.setBackground(Color.decode("#4fc3f7"));
             }
       
             public void mouseExited(java.awt.event.MouseEvent evt) {
               btnClose.setBackground(UIManager.getColor("control"));
             }
        });
		
		btnReport.setFont(new Font("Serif",Font.BOLD,20));
		btnReport.setCursor(new Cursor(Cursor.HAND_CURSOR));
		//btnModificar.setForeground(Color.WHITE);
		btnReport.setFocusPainted(true);
		btnReport.setBorderPainted(false);
		btnReport.addMouseListener(new java.awt.event.MouseAdapter() {
             public void mouseEntered(java.awt.event.MouseEvent evt) {
               btnReport.setBackground(Color.decode("#4fc3f7"));
             }
       
             public void mouseExited(java.awt.event.MouseEvent evt) {
               btnReport.setBackground(UIManager.getColor("control"));
             }
        });
		
		
		//lbl = new JLabel(""); 
	}
	
	
	public static void getVisibleTabla(String valor){
        
		 
	}
	
	public static void getVisibleLog(boolean status){
		btnLog.setVisible(status); 
	}	
	public void addElement(){
		
		
		this.setLayout(null); 
        this.add(scroll); 
		this.add(btnInsert); 
		this.add(txtBuscar); 
		this.add(lblBuscar); 
		this.add(btnModificar); 
		//this.add(btnConsult); 
		//this.add(btnClose); 
		this.add(btnLog); 
		this.add(btnReport);
		
	}
	
	public void actualizarTabla(){
		controller.getEspeciesAcuaticasTModel(this);
	}
	public void imgFondo(){
	  
	}
	public void filtrar(){ 
	    String buscar = txtBuscar.getText(); 
		controller.getEspecieAcuaticaTModel(this,buscar); 
	}
	public void changePanels(String panelChange){
		panelCard.show(this, panelChange); 
	}
	public void setUpdateInfo(EspeciesAcuaticas especieAcuatica){
		vistaGuardar.setUpdateInfo(especieAcuatica);
	}
	
	public void evenClick(){
		
		btnInsert.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				filtrar(); 
				//txtBuscar.setText("");
			}
		});
		btnModificar.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				actualizarTabla(); 
				txtBuscar.setText(""); 
			}
		}); 
		
		
		btnLog.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ev){
					Vista.changePanelN(Vista.LOGS);
				}
			});
		/**
		btnConsult.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				//JOptionPane.showMessageDialog(null, "serch"); 
				//
				//ArrayList<Personas> persona = new ArrayList<Personas>(); 
				//
				//modelo = new DefaultTableModel(); 
				//JTable tabl = new JTable(modelo); 
				
				//controller.mostrarPersonas(); 
				String nombreControll = controller.getNombre();
				
				System.out.println("Nombre en vista: "+nombreControll); 
				
			}
		}); */
		
		btnClose.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				try{
					 
					
				}catch(Exception ex){
					
				}
				JOptionPane.showMessageDialog(null, "close session"); 
				
			}
		}); 
		//@SuppressWarnings({ "unchecked", "rawtypes" })
		btnReport.addActionListener(new ActionListener(){
					
				public void actionPerformed(ActionEvent ev){
					String path = "iReport-5.6.0\\report-s.jasper";
				JasperReport jr = null;
				Map<String, Object> titulo = new HashMap<>();
				titulo.put("titulo", "Sistema Planeta");
				try{
					Controller re = new Controller();
					jr = (JasperReport)JRLoader.loadObjectFromFile(path);
					FileInputStream report = new FileInputStream(path);
					JasperPrint jp = JasperFillManager.fillReport(report, titulo, re.getConection());
					JasperViewer jv = new JasperViewer(jp,false); 
					jv.setTitle(path);
					jv.setVisible(true);
					
				}catch(Exception ex){
					System.out.println("ERROR: " + ex); 
				}
		
				
				JOptionPane.showMessageDialog(null," button has been pressed"); 
				
				}
			});
			
		
		//btnReporte.addActionListener(btnReport);
		
	}
	 
	
	public void loadTable(TableModel tmodel, int [] removeColumn){
		tabla.setModel(tmodel);
		for(int columnIndex : removeColumn){
			tabla.removeColumn(tabla.getColumnModel().getColumn(columnIndex));
		}
	}
	
}