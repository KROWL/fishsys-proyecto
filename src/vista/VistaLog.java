/**
   
   @author Kirlian Ortiz 
   @descripcion vistaUtilities 
   
**/ 
package vista; 

import javax.swing.JFrame; 
import javax.swing.ImageIcon; 
import java.awt.Image;  
import java.awt.Dimension; 
import java.awt.Toolkit; 
import java.awt.event.ActionListener; 
import java.awt.event.ActionEvent;  
import javax.swing.JLabel; 
import javax.swing.JPanel; 
import java.awt.Color; 
import java.awt.Graphics; 
import javax.swing.JTable; 
import javax.swing.table.*;  
import java.awt.BorderLayout; 
import javax.swing.BorderFactory; 
import javax.swing.JPanel; 
import javax.swing.JTextField; 
import javax.swing.JButton; 
import java.awt.event.MouseAdapter; 
import java.awt.event.MouseEvent; 
import java.awt.Cursor; 
import java.awt.Font;
import java.awt.Graphics2D; 
import java.awt.GradientPaint; 
import java.awt.RenderingHints; 
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.UIManager;
import javax.swing.JToolTip;
import javax.swing.JOptionPane;
import javax.swing.Icon; 
import javax.swing.JScrollPane;
import java.lang.Object; 
import javax.swing.table.DefaultTableModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener; 
import javax.swing.event.ListSelectionEvent; 
import java.util.ArrayList; 
import dominio.controller.Controller;
import dominio.Registro;
import dominio.EspeciesAcuaticas; 


public class VistaLog extends JPanel implements ITable{
	
	private JButton btnInsert = null; 
	private JButton btnModificar = null; 
	private JButton btnConsult = null; 
	private static JButton btnLogBack = null; 
	private JLabel labelIMG = null;	
	private JButton btnClose = null; 
	private Icon iconIMG = null; 
	private ImageIcon imagen = null; 
	private Image img = null; 
	private JLabel lblBuscar = null; 
	private Vista vista =null;
	private JTable tabla = null; 
	private JTable tablaLogs = null; 
	private JScrollPane scrollLogs = null; 
	private JScrollPane scroll = null; 
	private JTextField txtBuscar = null; 
	private VistaMostrar vistaMostrar = null; 
	
	private static String LOGS = "log"; 
	private CardLayout panelCard = null; 
	
	private Controller controller = null; 
	
	
	
	public VistaLog(Vista vista){
		
		this.setLayout(new BorderLayout()); 
		this.vista = vista;
		
		
		addVentana(); 
		addElement(); 
		//cambiarColorFondo(); 
		evenClick(); 
		imgFondo(); 
		JPanel panel = new JPanel(); 
		
	    this.setBorder(BorderFactory.createTitledBorder("Mostrar datos")); 
	    //this.add(new JLabel("Label")); 22,145,217
	
		this.setBackground(new Color(77,176,230));
		
	} 
	
	
	public void addVentana(){
		
		/* getClass.getResource() */
		ImageIcon imgSave = new ImageIcon(getClass().getResource("img/search24x24_01.png")); 
		ImageIcon imgUpdate = new ImageIcon(getClass().getResource("img/update0124x24.png")); 
		//ImageIcon imgSearch = new ImageIcon("img/grid_32x32.png"); 
		ImageIcon imgClose = new ImageIcon(getClass().getResource("img/close16x16.png")); 
		ImageIcon imgLogs = new ImageIcon(getClass().getResource("img/arrowback.png")); 
		
		lblBuscar = new JLabel("Buscar Nombre session"); 
		txtBuscar = new JTextField(); 
		
		btnInsert = new JButton("Buscar", imgSave);
		btnModificar = new JButton("Refresh", imgUpdate);
		//btnConsult = new JButton("Mostrar", imgSearch); 
		btnClose = new JButton("cerrar session", imgClose); 
		btnLogBack = new JButton("Back", imgLogs); 
		
		controller = new Controller(); 
		
		lblBuscar.setBounds(20, 70, 150, 25); 
		txtBuscar.setBounds(157, 70, 200, 25); 
		
		txtBuscar.setToolTipText("buscar usuarios");
		btnLogBack.setToolTipText("ir a tras");
		btnInsert.setToolTipText("buscar usuario");
		btnModificar.setToolTipText("refrescar tabla");
		
		
		
		btnLogBack.setBounds(100, 400, 170, 35); 
		btnInsert.setBounds(310, 400, 170,35); 
		//btnConsult.setBounds(510, 400, 170, 35); 
		btnClose.setBounds(600, 20, 120, 25); 
		btnModificar.setBounds(510, 400, 170,35); 
		
		tabla = new JTable(){
		    @Override
		       public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		           Component component = super.prepareRenderer(renderer, row, column);
		           int rendererWidth = component.getPreferredSize().width;
		           TableColumn tableColumn = getColumnModel().getColumn(column);
		           tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
		           return component;
		        }
		};
		
		tabla.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent lse) {
		        if (!lse.getValueIsAdjusting()) {
					for(int i =0;i<tabla.getModel().getColumnCount();i++){
						if(tabla.getSelectedRowCount()>0){
								Registro log = new Registro();
								
								
							}
						}
						
					}
		    }
		});
		controller.getLogsTModel(this);
		
		tabla.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		scroll = new JScrollPane(tabla); 
		scroll.setHorizontalScrollBarPolicy(javax.swing.JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS); 
		scroll.setVerticalScrollBarPolicy(javax.swing.JScrollPane.VERTICAL_SCROLLBAR_ALWAYS); 
		scroll.setViewportView(tabla); 
		
		//tabla.setAutoResizeModel(1); 
		tabla.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); 
		tabla.getColumnModel().getColumn(0).setPreferredWidth(77);
		tabla.getColumnModel().getColumn(1).setPreferredWidth(100);
		tabla.getColumnModel().getColumn(2).setPreferredWidth(100);
		tabla.getColumnModel().getColumn(3).setPreferredWidth(120);
		
		scroll.setBounds(150, 150, 500, 200); 
		btnInsert.setFont(new Font("Serif",Font.BOLD,20));
        btnInsert.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnInsert.setBorderPainted(false);
		btnInsert.addMouseListener(new java.awt.event.MouseAdapter() {
           public void mouseEntered(java.awt.event.MouseEvent evt) {
             btnInsert.setBackground(Color.decode("#4fc3f7"));
		   }
		   public void mouseExited(java.awt.event.MouseEvent evt) {
			 btnInsert.setBackground(UIManager.getColor("control"));
		   }
		});
		
		btnModificar.setFont(new Font("Serif",Font.BOLD,20));
		btnModificar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		//btnModificar.setForeground(Color.WHITE);
		btnModificar.setFocusPainted(true);
		btnModificar.setBorderPainted(false);
		btnModificar.addMouseListener(new java.awt.event.MouseAdapter() {
             public void mouseEntered(java.awt.event.MouseEvent evt) {
               btnModificar.setBackground(Color.decode("#4fc3f7"));
             }
       
             public void mouseExited(java.awt.event.MouseEvent evt) {
               btnModificar.setBackground(UIManager.getColor("control"));
             }
        });
		btnLogBack.setFont(new Font("Serif",Font.BOLD,25));
		btnLogBack.setCursor(new Cursor(Cursor.HAND_CURSOR));
		//btnModificar.setForeground(Color.WHITE);
		btnLogBack.setFocusPainted(true);
		btnLogBack.setBorderPainted(false);
		btnLogBack.addMouseListener(new java.awt.event.MouseAdapter() {
             public void mouseEntered(java.awt.event.MouseEvent evt) {
               btnLogBack.setBackground(Color.decode("#4fc3f7"));
             }
       
             public void mouseExited(java.awt.event.MouseEvent evt) {
               btnLogBack.setBackground(UIManager.getColor("control"));
             }
        });
		/**
		btnConsult.setFont(new Font("Serif",Font.BOLD,20));
		btnConsult.setCursor(new Cursor(Cursor.HAND_CURSOR));
		//btnConsult.setForeground(Color.WHITE);
		btnConsult.setFocusPainted(true);
		btnConsult.setBorderPainted(false);
		btnConsult.addMouseListener(new java.awt.event.MouseAdapter() {
             public void mouseEntered(java.awt.event.MouseEvent evt) {
               btnConsult.setBackground(Color.gray);
             }
       
             public void mouseExited(java.awt.event.MouseEvent evt) {
               btnConsult.setBackground(UIManager.getColor("control"));
             }
        }); */
		
		btnClose.setFont(new Font("Serif",Font.PLAIN,12));
		btnClose.setCursor(new Cursor(Cursor.HAND_CURSOR));
		//btnModificar.setForeground(Color.WHITE);
		btnClose.setFocusPainted(true);
		btnClose.setBorderPainted(false);
		btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
             public void mouseEntered(java.awt.event.MouseEvent evt) {
               btnClose.setBackground(Color.decode("#4fc3f7"));
             }
       
             public void mouseExited(java.awt.event.MouseEvent evt) {
               btnClose.setBackground(UIManager.getColor("control"));
             }
        });
		
		
		
		//lbl = new JLabel(""); 
	}
	
	public void addElement(){
		
		this.setLayout(null); 
		
        this.add(scroll); 
		this.add(btnLogBack); 
		this.add(txtBuscar); 
		this.add(lblBuscar); 
		this.add(btnModificar); 
		//this.add(btnConsult); 
		//this.add(btnClose); 
		this.add(btnInsert); 
	}
	
	public void actualizarTabla(){
		controller.getLogsTModel(this);
	}
	
	public static void getVisibleBack(boolean status){
		btnLogBack.setVisible(status);
	}
	
	public void imgFondo(){
	  
	}
	public void filtrar(){ 
	    String buscar = txtBuscar.getText(); 
		controller.getLogTModel(this,buscar); 
		System.out.println("txt: "+buscar);
	}
	public void evenClick(){
		
		btnInsert.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				filtrar(); 
				//txtBuscar.setText("");
			}
		});
		btnModificar.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				actualizarTabla(); 
				txtBuscar.setText(""); 
			}
		}); 
		
		btnClose.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				
			}
		}); 
		btnLogBack.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ev){
					Vista.changePanelN(Vista.MOSTRAR);
				}
			}); 
			
		
		
	}
	
	public void loadTable(TableModel tmodel, int [] removeColumn){
		tabla.setModel(tmodel);
		for(int columnIndex : removeColumn){
			tabla.removeColumn(tabla.getColumnModel().getColumn(columnIndex));
		}
	}
	
}