/**
   
   @author Kirlian Ortiz 
   @decripcion skeleton 
   @version 0 
   
**/ 

package vista;  

import java.sql.*; 
import javax.swing.*; 
import java.awt.*; 
import java.awt.event.*; 
import java.util.*; 
import javax.swing.BorderFactory; 
import javax.swing.border.*;  
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.JToolTip;
import javax.swing.plaf.ColorUIResource;
import java.awt.event.ActionListener; 


import javafx.scene.Scene;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
//import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;


import javax.swing.UIManager.LookAndFeelInfo;

import dominio.controller.Controller; 


public class LoginVista extends VentanaUtilidades{
	
	public JLabel userLabel; 
	public JLabel passwordLabel; 
	public JTextField userText; 
	public JPasswordField passwordText; 
	public JButton btnAceptar; 
	public JPanel panel; 
	public JOptionPane pane; 
	public JLabel tituloLabel; 
	 
	
	public LoginVista(){
		
		//this.setUndecorated(true); 
		ImageIcon iconPez = new ImageIcon("pez03.jpg");
		tituloLabel = new JLabel("LOGIN..FishSys "); 
		tituloLabel.setBounds(getWidth()/6, 240, 600, 30); 
		tituloLabel.setFont(new Font("Serif", Font.BOLD, 30 )); 
		tituloLabel.setForeground(Color.RED);  
		String path = "img/imagen.png";  
        java.net.URL url = this.getClass().getResource(path);  
        ImageIcon icon = new ImageIcon(url);  
         // UIManager.put("ToolTip.background",new Color(0x40,0x51,0x67,0x40));
UIManager.put("ToolTip.background",new ColorUIResource(0x60,0x6a,0x76));
UIManager.put("ToolTip.backgroundInactive",new ColorUIResource(0x60,0x6a,0x76));
        JLabel label = new JLabel("-FishSys");  
        label.setIcon(icon);
		
		// JScrollPane scrollPane; 
		// ImageIcon ico; 
		 
		
        // ico = new ImageIcon("pez03.jpg");
        
        // JPanel panel= new JPanel(){
        // public void paintComponent(Graphics g)
        // {
        // super.paintComponent(g);        
        // g.drawImage(ico.getImage(), 0, 0, null);
        // super.setOpaque(false);
        // }
        // };
        
        // panel.setOpaque(false);
        //scrollPane = new JScrollPane(panel);
        //getContentPane().add(panel);
		//JLabel lblTitulo = new JLabel("Bienvenido! Valide sus datos");
		//lblTitulo.setBounds(getWidth()/6, 10, 600, 30);
	    //lblTitulo.setFont(new Font("Serif", Font.BOLD, 30));
	    //lblTitulo.setForeground(Color.WHITE);
		//label 
		userLabel = new JLabel("Usuario "); 
		passwordLabel = new JLabel("contrase\u00F1a"); 
		
		//text 
		userText = new JTextField(10); 
		passwordText = new JPasswordField(10); 
		
		//btn 
		btnAceptar = new JButton("iniciar session"); 
		//tooltips textField
	        //UIManager.put("ToolTip.background", new ColorUIResource(255, 247, 200));

		passwordText.setToolTipText("Escriba password"); 
		userText.setToolTipText("<html><div style='margin:0 -3 0 -3; padding: 0 3 0 3;'>Escriba nombre</div></html>");
		//metodos 

		super.crearVentana("Login"); 
		super.cambiarIcono(); 
		 //setIconImage(new ImageIcon(getClass().getResource("img/img01.jpg")).getImage());
		
		addOpciones(); 
		addElementos(); 
		super.imgFondo(); 
		
		//paintIcon 
//-fx-background-color: -fx-shadow-highlight-color, -fx-outer-border, -fx-inner-border, -fx-body-color;-fx-background-insets: 0 0 -1 0, 0, 1, 2;-fx-background-radius: 3px, 3px, 2px, 1px;
		/* panel.add(userLabel); 
		panel.add(userText); 
		panel.add(passwordLabel); 
		panel.add(passwordText); 
		panel.add(btnAceptar); 
		
		this.setContentPane(panel);  */
		
		//add evento colocar en un metodo en Ventana GEn
        btnAceptar.addActionListener(this);       	
		
		//cerrar ventana 
		//Nota: NO usar EXIT_ON_CLOSE cando implementas windowsclosing 
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		//title 
		//this.setTitle("Vista"); 
		this.setSize(700, 400); 
		this.setResizable(false);
		this.setVisible(true); 
		//colocarlo siempre de ultimo
		this.setLocationRelativeTo(null); 

	}
	
	@Override  
	public void addOpciones(){
		
		//JPanel panel; 
		 
		//panel.add(new JLabel("Label")); 
		//this.setIconImage(Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("img/imagen.png"))); 
		
		//ImageIcon icon = new ImageIcon("img/img2.png"); 
		//this.setIconImage(icon.getImage());
		
		//this.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
		//panel.setBackground(Color.ORANGE); 
        
		//this.setLayout(null); 
		//10, 30, 600, 100 
		//int x, int y, int width, int height 
		userLabel.setBounds(100, 20, 120, 25); 
		passwordLabel.setBounds(90, 60, 120, 25); 
		userText.setBounds(280, 20, 276, 26); 
		passwordText.setBounds(280, 60, 276, 26); 
		
		btnAceptar.setBounds(460, 110, 140, 25); 
		userLabel.setFont(new Font("Serif",Font.PLAIN,20));
		passwordLabel.setFont(new Font("Serif",Font.PLAIN,20));
	}
	
	@Override
	public void addElementos(){
		
		//this.setIconImage(Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("img/imagen.png"))); 
		
		//JPanel panel = new JPanel(); 
		panel = new JPanel(new BorderLayout()) {
           protected void paintComponent(Graphics grphcs) {
              super.paintComponent(grphcs);
              Graphics2D g2d = (Graphics2D) grphcs;
              //g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
              GradientPaint gp = new GradientPaint(0, 0,
                new java.awt.Color(45, 222, 222), 0, getHeight(),
                new java.awt.Color(67, 130, 179));
                g2d.setPaint(gp);
                g2d.fillRect(0, 0, getWidth(), getHeight()); 
           }
        };
		panel.setLayout(null); 
        
		panel.setBorder(BorderFactory.createTitledBorder("LoginFishSys"));
		
		panel.add(tituloLabel); 
		panel.add(userLabel); 
		panel.add(passwordLabel); 
		panel.add(userText); 
		panel.add(passwordText); 
		panel.add(btnAceptar);
		
		add(panel); 
	}
	
	//icon 
	//public void login(){} 
	
	@Override 
	public void actionPerformed(ActionEvent e){
	    
	   String us = ""; 
	   String pas = ""; 
	   
	   if(e.getSource() == btnAceptar){
		    	   
		   us = userText.getText().toString(); 
		   pas = String.valueOf(passwordText.getPassword()); 
		   
		   
		   boolean valida; 
		   Controller user = new Controller(); 
		   
		   //evaluar si los campos estan vacios 
		   if(us.equals("") || pas.equals("")){
			   
			  JOptionPane.showMessageDialog(null, "El nombre de usuario/ contrasena estan vacios"); 
			  //userText.setText(""); 
			  //passwordText.setText(""); 
			  
		   }else{
			   
			   valida = user.validarUsuario(us, pas); 
			   //validar los campos ya fills 
			   
			   if(valida){
			      //esconde la ventana y no cierra toda la  app 
				  //se puede usar dispose() 
				  //el obj va ha gastar mas tiempo en un CRUD con setvisible(true) 
				  dispose();
			   
		       }else{
			      //limpia campos  
			      userText.setText(""); 
			      passwordText.setText(""); 
		       } 
		   }
		   
		   /* if(valida){
					hide(); 
					//userText.setText(""); 
			        //passwordText.setText(""); 
					
		   }else{ 
		      
			   
					
		   } */
		   
	   }
	   
	   
	   
		
	}
}