/**
   
   @author Kirlian Ortiz 
   @version 0 
   @descripcion Class Select Table 
   
**/ 


package vista; 

import javax.swing.*; 
import java.awt.*; 
import javax.swing.JTable;
import javax.swing.table.*;
import javax.swing.JScrollPane;
import java.awt.event.*; 
import java.sql.*; 

import dominio.controller.Controller; 
import DAL.InterfazDAL; 
import dominio.Usuario; 

public class SelectTables extends JFrame implements ActionListener{
	
	public DefaultTableModel modelo; 
	public JTable tabla;
	public JScrollPane scrollPane;
	public JScrollPane scroll; 
	public JComboBox <String> combo; 
	public JLabel labelTabla;
	public JPanel panel;
	
	public SelectTables(){
		
		//JLabel label = new JLabel("selecione tabla"); 
		panel = new JPanel();
		
		panel = new JPanel(new BorderLayout()) {
           protected void paintComponent(Graphics grphcs) {
              super.paintComponent(grphcs);
              Graphics2D g2d = (Graphics2D) grphcs;
              //g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
              GradientPaint gp = new GradientPaint(0, 0,
                new java.awt.Color(45, 222, 222), 0, getHeight(),
                new java.awt.Color(67, 130, 179));
                g2d.setPaint(gp);
                g2d.fillRect(0, 0, getWidth(), getHeight()); 
           }
        };
		panel.setLayout(null);
		
		String usuario = ""; 
		String contrasena = ""; 
		
		//Usuario user = new Usuario(usuario, contrasena); 
		Controller ua = new Controller(); 
		
		labelTabla = new JLabel("Seleccione tabla"); 
		
		
		//String textUser = user.java.lang.String getUserName; 
		
		JLabel nombUser = new JLabel(ua.getUsuario()); 
		JLabel textLabel = new JLabel("Nombre Usuario: "); 
		
		//para el combobox 
		labelTabla.setBounds(10, 20, 100, 20); 
		nombUser.setBounds(150, 50, 100, 20); 
		textLabel.setBounds(10, 50, 100, 20); 
		
		//panel.setBorder(BorderFactory.createTitledBorder("Tablas del usuario"));
		panel.add(labelTabla);
		mostrarTablas();
		panel.add(textLabel);
		panel.add(nombUser);
		
		combo.addActionListener(this); 
		this.setTitle("Consultar Tablas"); 
		cambiarIcono(); 
		imagenFondo(); 
		
		
		add(panel); 
		
		
		//panel taba 
		JPanel pnlTop = new JPanel(new BorderLayout()); 
        pnlTop.setBorder(BorderFactory.createTitledBorder("Tabla seleccionada")); 
		pnlTop.add(new JLabel("Label")); 
		
		modelo = new DefaultTableModel(); 
		tabla = new JTable(modelo); 
		
		scroll = new JScrollPane(tabla); 
		
		tabla.setPreferredScrollableViewportSize(new Dimension(600, 400)); 
		pnlTop.add(scroll, BorderLayout.CENTER); 
		
		pnlTop.setPreferredSize(new Dimension(200, 200)); 
		add(pnlTop, BorderLayout.SOUTH); 
		
		//scrollPane = new JScrollPane();
		//tabla = new JTable();
		//scrollPane.setPreferredSize(new Dimension (200,300));
		//scrollPane.add(tabla);
		//JPanel pnlTop = new JPanel();  //Imagen de Fondo
		//pnlTop.add(scrollPane);
		
		// public void paintComponent(Graphics g) {  
			  // Image img = Toolkit.getDefaultToolkit().getImage(  
			  // VentanaUtilidades.class.getResource("img/pezTrop.jpg"));  
			  // g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);  
			// }  
		// }; 
		
		//ImageIcon img = new ImageIcon("img/img2.png"); 
			//JLabel lbl = new JLabel(img); 
			//lbl.setBounds(200,10,500,300);
		//pnlTop.add(lbl); 

		//pnlTop.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(173, 173, 166)));
		//pnlTop.setPreferredSize(new Dimension(200, 200));
		//add(pnlTop);
		
		
		//validar cierre ventana 
	    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); 
	    addWindowListener(new CloseWindow()); 
		
		
		setSize(600, 400); 
		setVisible(true); 
		setResizable(true); 
	    setLocationRelativeTo(null); 
	} 
	
	public void cambiarIcono(){
		
		try{
		Image icono = Toolkit.getDefaultToolkit().getImage(getClass().getResource("img/img1.jpg")); 
		}catch(Exception e){}
	}
	public void imgFondo(){
	   
	   
	   setContentPane(new JLabel(new ImageIcon("pez03.jpg")));  
	    
   }
	public void imagenFondo(){
		
		getContentPane().add(new JPanel(){
			@Override 
			public void paintComponent(Graphics g){
				super.paintComponent(g); 
				
				ImageIcon imagen = new ImageIcon(getClass().getResource("img/img1.jpg")); 
				g.drawImage(imagen.getImage(), 0, 0, getWidth(), getHeight(), null); 
			}
		}); 
	} 
	//@Override 
	public void actionPerformed(ActionEvent e){
		
		String action = e.getActionCommand();
		
		JComboBox cb = (JComboBox)e.getSource(); 
		String selectCombo = (String)cb.getSelectedItem(); 
		limpiarTable(); 
		if(selectCombo.equals("COORDENADASGEOGRAFICAS") ){
			//String selectCombo = (String)combo.getSelectedItem(); 
			//JLabel selectTable = new JLabel(selectCombo); 
			//JOptionPane.showMessageDialog(null, "HOla"); 
		  //panel.setBackground(new Color(67,130,19)); 
		  try{
			  new Controller(this).selectTabla(selectCombo); 
		  }catch(Exception ex){}
			//JComboBox comboBox = (JComboBox) e.getSource(); 
			System.out.println("Selected Item  = " + cb.getSelectedItem());
            System.out.println("Action Command = " + e.getActionCommand());
		}else if(selectCombo.equals("ESPECIESACUATICAS")){
		  //Principal name =  throw Exception; 
		  
		  try{
			  new Controller(this).selectTabla(selectCombo); 
		  //new PlanetaDAO(this).updateTab(xselection); 
		  //new PlanetaDAO().mostrarTable(table,scroll,"Habitats"); 
		   
			  
		  }catch(Exception ex){} 
		  
	  } else if(selectCombo.equals("ESPECIESPORHABITATS")){
		  //Principal name =  throw Exception; 
		  
		  try{
			  new Controller(this).selectTabla(selectCombo); 
		  //new PlanetaDAO(this).updateTab(xselection); 
		  //new PlanetaDAO().mostrarTable(table,scroll,"Habitats"); 
		   
			  
		  }catch(Exception ex){} 
		  
	  } else if(selectCombo.equals("HABITATS")){
		  //Principal name =  throw Exception; 
		  
		  try{
			  new Controller(this).selectTabla(selectCombo); 
		  //new PlanetaDAO(this).updateTab(xselection); 
		  //new PlanetaDAO().mostrarTable(table,scroll,"Habitats"); 
		   
			  
		  }catch(Exception ex){} 
		  
	  } else if(selectCombo.equals("LOGS")){
		  //Principal name =  throw Exception; 
		  
		  try{
			  new Controller(this).selectTabla(selectCombo); 
		  //new PlanetaDAO(this).updateTab(xselection); 
		  //new PlanetaDAO().mostrarTable(table,scroll,"Habitats"); 
		   
			  
		  }catch(Exception ex){} 
		  
	  } else if(selectCombo.equals("TAXONOMIA")){
		  //Principal name =  throw Exception; 
		  
		  try{
			  new Controller(this).selectTabla(selectCombo); 
		  //new PlanetaDAO(this).updateTab(xselection); 
		  //new PlanetaDAO().mostrarTable(table,scroll,"Habitats"); 
		   
			  
		  }catch(Exception ex){} 
		  
	  } 
	  else if(selectCombo.equals("ZONASGEOS")){
		  //Principal name =  throw Exception; 
		  
		  try{
			  new Controller(this).selectTabla(selectCombo); 
		  //new PlanetaDAO(this).updateTab(xselection); 
		  //new PlanetaDAO().mostrarTable(table,scroll,"Habitats"); 
		   
			  
		  }catch(Exception ex){} 
		  
	  }
		 
	} 
	
	
	public void mostrarTablas(){
		
		//String[] nombTabla = {"EspeciesAcuaticas", "Habitats", "EspeciesPorHAbitas", "Taxonomias", "LOGS"}; 
		
		try{
			
			InterfazDAL dal = new InterfazDAL(); 
			String[] datos = new String[]{}; 
			//dal.executeGetTables(); 
			combo = new JComboBox <String>(dal.executeGetTables()); 
			combo.addItem("Visible String 1");
			/* DefaultTableModel modelo = new DefaultTableModel(); 
			JTable tab = new JTable(modelo); 
			
			JScrollPane scroll = new JScrollPane(tab); 
			
			tab.setPreferredScrollableViewportSize(new Dimension(400, 200)); 
			getContentPane().add(scroll, BorderLayout.CENTER);
			
			combo.addActionListener(new ActionListener(){
				
				public void actionPerformed(ActionEvent e){
					
					JComboBox comboBox = (JComboBox) e.getSource(); 
					comboBox = combo; 
					//
					//String comboString = String.valueOf(combo.getSelectedItem());
					System.out.println("Selected Item  = " + combo.getSelectedItem()); 
					System.out.println("Action Command = " + e.getActionCommand());
					
					
					try{ 
					   InterfazDAL dal = new InterfazDAL(); 
					    
						dal.selectTablas(String.valueOf(combo.getSelectedItem())); 
						
						
						/ for(int i=0; i<tabla1.length; i++){
							
							modelo.addColumn(tabla1[])
							
						} /
					    //JTable table = new JTable(tabla1); 
						
						
		            }catch(SQLException sql){
						
					}
				}
				
			});  */
			
		}catch(SQLException e){
			
		}
		 
		//combo.setModel(new DefaultComboBoxModel(new String[]{})); 
		
		 
		combo.setBounds(150,20, 170, 20); 
		
		panel.add(combo); 
		
	}
	public void limpiarTable(){
		DefaultTableModel model = (DefaultTableModel)tabla.getModel(); 
		model.setColumnCount(0); 
		model.setRowCount(0);
	}
	public void mostrarTablasVista(String tabla){
		
		 
	} 
	
}