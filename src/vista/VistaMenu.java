/**
   
   @author Kirlian Ortiz 
   @descripcion vistaUtilities 
   
**/ 
package vista; 

import javax.swing.JFrame; 
import javax.swing.ImageIcon; 
import java.awt.Image;  
import java.awt.Dimension; 
import java.awt.Toolkit; 
import java.awt.event.ActionListener; 
import java.awt.event.ActionEvent;  
import javax.swing.JLabel; 
import javax.swing.JPanel; 
import java.awt.Color; 
import java.awt.Graphics; 
import javax.swing.JTable; 
import javax.swing.table.*;  
import java.awt.BorderLayout; 
import javax.swing.BorderFactory; 
import javax.swing.JPanel; 
import javax.swing.JTextField; 
import javax.swing.JButton; 
import java.awt.event.MouseAdapter; 
import java.awt.event.MouseEvent; 
import java.awt.Cursor; 
import java.awt.Font;
import java.awt.Graphics2D; 
import java.awt.GradientPaint; 
import java.awt.RenderingHints; 
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.UIManager;
import javax.swing.JOptionPane;
import javax.swing.Icon; 

import dominio.controller.Controller; 

public class VistaMenu extends JPanel{
	
	private JButton btnInsert = null; 
	private JButton btnModificar = null; 
	private JButton btnConsult = null; 
	private JLabel labelIMG = null;	
	private JButton btnClose = null; 
	private Icon iconIMG = null; 
	private ImageIcon imagen = null; 
	private Image img = null; 
	
	
	private JPanel cards = null;
	private CardLayout cardMain=null; 
    final static String INSERT = "insert";
    final static String UPDATE = "update";
	public VistaMenu(){
		
	    addVentana(); 
		addElement(); 
		//cambiarColorFondo(); 
		evenClick(); 
		imgFondo(); 
		JPanel panel = new JPanel(); 
		cardMenu(); 
	    this.setBorder(BorderFactory.createTitledBorder("Menu")); 
	    //this.add(new JLabel("Label")); 22,145,217
	
		this.setBackground(new Color(77,176,230));
		
	} 
	
		public void addVentana(){
			
			
			//img btns 
			ImageIcon imgSave = new ImageIcon(getClass().getResource("img/save32x32_01.png")); 
			ImageIcon imgUpdate = new ImageIcon(getClass().getResource("img/update0124x24.png")); 
			ImageIcon imgSearch = new ImageIcon(getClass().getResource("img/grid_32x32.png")); 
			imagen = new ImageIcon(getClass().getResource("img/pez03.jpg")); 
			//getClass().getResource()
			btnInsert = new JButton("Insertar Especie", imgSave);
			btnModificar = new JButton("Modificar", imgUpdate);
			btnConsult = new JButton("Mostrar", imgSearch); 
			labelIMG = new JLabel(imagen); 
			 

			//setBounds btns 
			btnInsert.setBounds(45, 50, 150, 50); 
			btnModificar.setBounds(45, 140, 150,50); 
			btnConsult.setBounds(45, 240, 150, 50); 
			
			labelIMG.setBounds(5, 250, 243, 300); 
			
			//styles btns 
			btnInsert.setFont(new Font("Serif",Font.BOLD,20));
			btnInsert.setCursor(new Cursor(Cursor.HAND_CURSOR));
			//btnInsert.setForeground(Color.WHITE);
			//btnInsert.setFocusPainted(false);
			btnInsert.setBorderPainted(false);
			btnInsert.addMouseListener(new java.awt.event.MouseAdapter() {
			   public void mouseEntered(java.awt.event.MouseEvent evt) {
				 btnInsert.setBackground(Color.decode("#4fc3f7"));
			   }
			   public void mouseExited(java.awt.event.MouseEvent evt) {
				 btnInsert.setBackground(UIManager.getColor("control"));
			   }
			});
			
			btnModificar.setFont(new Font("Serif",Font.BOLD,20));
			btnModificar.setCursor(new Cursor(Cursor.HAND_CURSOR));
			//btnModificar.setForeground(Color.WHITE);
			btnModificar.setFocusPainted(true);
			btnModificar.setBorderPainted(false);
			btnModificar.addMouseListener(new java.awt.event.MouseAdapter() {
				 public void mouseEntered(java.awt.event.MouseEvent evt) {
				   btnModificar.setBackground(Color.decode("#4fc3f7"));
				 }
		   
				 public void mouseExited(java.awt.event.MouseEvent evt) {
				   btnModificar.setBackground(UIManager.getColor("control"));
				 }
			});
			
			btnConsult.setFont(new Font("Serif",Font.BOLD,20));
			btnConsult.setCursor(new Cursor(Cursor.HAND_CURSOR));
			//btnConsult.setForeground(Color.WHITE);
			btnConsult.setFocusPainted(true);
			btnConsult.setBorderPainted(false);
			btnConsult.addMouseListener(new java.awt.event.MouseAdapter() {
				 public void mouseEntered(java.awt.event.MouseEvent evt) {
				   btnConsult.setBackground(Color.decode("#4fc3f7"));
				 }
		   
				 public void mouseExited(java.awt.event.MouseEvent evt) {
				   btnConsult.setBackground(UIManager.getColor("control"));
				 }
			});
			
			
			
			//lbl = new JLabel(""); 
		}
		
		public void addElement(){
			
			this.setLayout(null); 
			
			this.add(btnInsert); 
			this.add(btnModificar); 
			this.add(btnConsult); 
			this.add(labelIMG); 
		}
		
		public void imgFondo(){
		  
		}
		public void evenClick(){
			
			btnInsert.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ev){
					JOptionPane.showMessageDialog(null, "Insert"); 
				}
			});
			btnModificar.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ev){
					cardMain = (CardLayout)(cards.getLayout()); 
					cardMain.show(cards, UPDATE);  
				}
			});
			btnConsult.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ev){
					JOptionPane.showMessageDialog(null, "serch"); 
				}
			});
		}
		
		public void cardMenu(){
			//Create the panel that contains the "cards".
			cardMain = new CardLayout();
			cards = new JPanel(cardMain); 
			cards.add(new VistaInsert(), INSERT);
			cards.add(new VistaUpdate(), UPDATE); 
			
			
		}
}

