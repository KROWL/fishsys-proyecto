// These examples show the use of SET ROLE in JDBC statements.
  // The case normal form is visible in the SYS.SYSROLES system table.
  stmt.execute("SET ROLE admin");      -- case normal form: ADMIN
  stmt.execute("SET ROLE \"admin\"");  -- case normal form: admin
  stmt.execute("SET ROLE none");       -- special case

  PreparedStatement ps = conn.prepareStatement("SET ROLE ?");
  ps.setString(1, "  admin ");  -- on execute: case normal form: ADMIN
  ps.setString(1, "\"admin\""); -- on execute: case normal form: admin
  ps.setString(1, "none");      -- on execute: syntax error
  ps.setString(1, "\"none\"");  -- on execute: case normal form: none