/**
   
   @author Kirlian Ortiz 
   @descripcion vistaUtilities 
   
**/ 
package vista;

import javax.swing.JFrame; 
import java.awt.Frame; 
import javax.swing.ImageIcon; 
import java.awt.Image; 
import java.awt.Dimension; 
import java.awt.Toolkit; 
import java.awt.event.ActionListener; 
import java.awt.event.ActionEvent; 
import javax.swing.JLabel; 
import javax.swing.JPanel; 
import java.awt.Color; 
import java.awt.Graphics; 
import javax.swing.JTable; 
import javax.swing.table.*;  
import java.awt.BorderLayout; 
import javax.swing.BorderFactory; 
import javax.swing.JPanel; 
import javax.swing.BoxLayout; 
import javax.swing.JTextField; 
import javax.swing.JButton; 
import java.awt.event.MouseAdapter; 
import java.awt.event.MouseEvent; 
import java.awt.Cursor; 
import java.awt.Font;
import java.awt.Graphics2D; 
import java.awt.GradientPaint; 
import java.awt.RenderingHints; 
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.UIManager;
import javax.swing.JOptionPane;
import javax.swing.Icon; 
import javax.swing.JButton; 
import java.lang.Runtime;
import java.io.File;
import java.awt.Desktop;
import java.io.IOException;
import java.lang.Process;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.awt.event.WindowStateListener;

import javax.swing.JMenu; 
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolTip;
import dominio.controller.Controller;
import dominio.EspeciesAcuaticas; 
import dominio.Registro;
public class Vista extends VistaUtilities{
   
   
   private static JButton btnInsert = null; 
   private static JButton btnModificar = null; 
   private static JButton btnConsult = null; 
   private JLabel labelIMG = null;	
   private JButton btnClose = null; 
   private Icon iconIMG = null; 
   private ImageIcon imagen = null; 
   private Image img = null; 
   private VistaInsert vistaInsert;  
   private ViewGuardar vistaGuardar ; 
   private VistaMostrar vistaMostrar;
   private VistaMostrarB vistaMostrarB;
   
   private VistaLog vistaLog; 
   private JButton btnLog = null; 
   
   private static CardLayout panelCard =null;
   private JPanel panelMenu     =null;
   
   public static String INSERT = "Insert";
   public static String UPDATE = "update";
   public static String MOSTRAR = "mostrar"; 
   public static String MOSTRARB = "mostrarb"; 
   public static String LOGS = "log"; 
   private static JFrame frame = null;
   private static JPanel panel = null; 
   private JPanel card1 = null; 
   
   private JLabel label = null;
   private ImageIcon imgSave   = null; 
   private ImageIcon imgUpdate = null; 
   private ImageIcon imgSearch = null; 
   private ImageIcon imgLog    = null;
   private JMenuBar barr     = null;
   private JMenuItem item_help=null;
   private JMenuItem close_u=null; 
   private JMenu     menu = null;
   private Vista vista;
   public Vista(){
         
		/*  try{
			 new InterfazDAL().enviarValidacion();
		 }catch(Exception ex){
			 ex.printStackTrace();
		 } */
		
		//super.CambiarIcono();
		
		initElements(); 
		addVentana(); 
		addElement(); 
		evenClick(); 
		closeWindows(this,frame); 
	    
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
		
		//frame.setIconImage(new ImageIcon("img/fishy.png").getImage());
		frame.setIconImage(new ImageIcon(getClass().getResource("img/img01.jpg")).getImage());
	    
	    frame.setTitle("FishSys v1.4"); 
		frame.setSize(1020,540); 
		frame.setVisible(true);
		//this.setLocation(300,100); 
        //this.setVisible(true); 
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
   }
   
   public void initElements(){

	   vistaInsert  = new VistaInsert(this );  
	   vistaGuardar = new ViewGuardar(this);  
	   vistaMostrar = new VistaMostrar(this); 
	   vistaMostrarB = new VistaMostrarB(this); 
	   
	   //vistaLog     = new VistaLog(this); 
	   vistaLog     = new VistaLog(this);
	   panelMenu = new JPanel(); 
	   panelCard = new CardLayout();
	   panel     = new JPanel(panelCard);
	   card1     = new JPanel();
	   frame     = new JFrame();
	   barr      = new JMenuBar();
	   menu      = new JMenu("help");
	   item_help = new JMenuItem("manual de usuario");
	   close_u   = new JMenuItem("cerrar session");
	   
	   //img btns 
	   imgSave   = new ImageIcon(getClass().getResource("img/save32x32_01.png")); 
	   imgUpdate = new ImageIcon(getClass().getResource("img/update0124x24.png")); 
	   imgSearch = new ImageIcon(getClass().getResource("img/grid_32x32.png")); 
	   imagen = new ImageIcon(getClass().getResource("img/pez03.jpg")); 
	   
	   imgLog = new ImageIcon(getClass().getResource("img/bitacora24x24.png"));
	   btnInsert = new JButton("Insertar ", imgSave);
	   btnModificar = new JButton("Modificar", imgUpdate);
	   btnConsult = new JButton("Mostrar", imgSearch); 
	   labelIMG = new JLabel(imagen);
	   String path = "img/pez03.jpg";  
        java.net.URL url = this.getClass().getResource(path); 
		
        ImageIcon icon = new ImageIcon(url);  
		  JLabel label = new JLabel("FishSys");
          label.setIcon(icon);
	   btnLog = new JButton("log", imgLog);
           
   }
   
   public void addVentana(){
			
			 
			int Xx = 39; 
			int Yy = 50; 
			
			frame.setLayout(new BorderLayout()); 
			
			panelMenu.setLayout(new BoxLayout(panelMenu, BoxLayout.X_AXIS));
            panelMenu.setPreferredSize(new Dimension(210, 100));
			//frame.setIconImage(new ImageIcon("fishy.png").getImage());
			panelMenu.setLayout(null); 
			panelMenu.setBackground(new Color(77,176,230));
			//this.setLayout(null); 
			panelMenu.setBorder(BorderFactory.createTitledBorder("Menu")); 
			
			//setBounds btns 
			btnInsert.setBounds   (Xx, Yy ,      150, 50); 
			btnModificar.setBounds(Xx, Yy + 100, 150, 50); 
			btnConsult.setBounds  (Xx, Yy + 200, 150, 50); 
			btnLog.setBounds(Xx, Yy+250,100,50);
			labelIMG.setBounds(10, 275, 190, 250); 
			
			
			//tooltip
			btnInsert.setToolTipText("insertar");
			btnModificar.setToolTipText("modificar");
			btnConsult.setToolTipText("consultar");
			menu.setToolTipText("manual de usuaio y cerrar session");
			item_help.setToolTipText("manual con imagenes");
			close_u.setToolTipText("cerrar session, ir login");
			
			//styles btns 
			btnInsert.setFont(new Font("Serif",Font.BOLD,20));
			btnInsert.setCursor(new Cursor(Cursor.HAND_CURSOR));
			//btnInsert.setForeground(Color.WHITE);
			//btnInsert.setFocusPainted(false);
			btnInsert.setBorderPainted(false);
			btnInsert.addMouseListener(new java.awt.event.MouseAdapter() {
			   public void mouseEntered(java.awt.event.MouseEvent evt) {
				 btnInsert.setBackground(Color.decode("#4fc3f7"));
			   }
			   public void mouseExited(java.awt.event.MouseEvent evt) {
				 btnInsert.setBackground(UIManager.getColor("control"));
			   }
			});
			
			btnModificar.setFont(new Font("Serif",Font.BOLD,20));
			btnModificar.setCursor(new Cursor(Cursor.HAND_CURSOR));
			//btnModificar.setForeground(Color.WHITE);
			btnModificar.setFocusPainted(true);
			btnModificar.setBorderPainted(false);
			btnModificar.addMouseListener(new java.awt.event.MouseAdapter() {
				 public void mouseEntered(java.awt.event.MouseEvent evt) {
				   btnModificar.setBackground(Color.decode("#4fc3f7"));
				 }
		   
				 public void mouseExited(java.awt.event.MouseEvent evt) {
				   btnModificar.setBackground(UIManager.getColor("control"));
				 }
			});
			
			btnConsult.setFont(new Font("Serif",Font.BOLD,20));
			btnConsult.setCursor(new Cursor(Cursor.HAND_CURSOR));
			//btnConsult.setForeground(Color.WHITE);
			btnConsult.setFocusPainted(true);
			btnConsult.setBorderPainted(false);
			btnConsult.addMouseListener(new java.awt.event.MouseAdapter() {
				 public void mouseEntered(java.awt.event.MouseEvent evt) {
				   btnConsult.setBackground(Color.decode("#4fc3f7"));
				 }
		   
				 public void mouseExited(java.awt.event.MouseEvent evt) {
				   btnConsult.setBackground(UIManager.getColor("control"));
				 }
			});
			btnLog.setFont(new Font("Serif",Font.BOLD,20));
			btnLog.setCursor(new Cursor(Cursor.HAND_CURSOR));
			//btnConsult.setForeground(Color.WHITE);
			btnLog.setFocusPainted(true);
			btnLog.setBorderPainted(false);
			btnLog.addMouseListener(new java.awt.event.MouseAdapter() {
				 public void mouseEntered(java.awt.event.MouseEvent evt) {
				   btnLog.setBackground(Color.decode("#4fc3f7"));
				 }
		   
				 public void mouseExited(java.awt.event.MouseEvent evt) {
				   btnLog.setBackground(UIManager.getColor("control"));
				 }
			});
			
			
			frame.addWindowStateListener(new WindowStateListener() {
                public void windowStateChanged(WindowEvent arg0) {
                       frame__windowStateChanged(arg0);
                }
            });
			
/* 			Border compound;
			barr.setBackground(Color.decode("#4fc3f7"));
			compound = BorderFactory.createCompoundBorder(
                          raisedbevel, loweredbevel);
			barr.setBorder(compound); */
			barr.setBorder(BorderFactory.createLineBorder(Color.decode("#4DB0EB")));
			menu.setFont(new Font("Serif",Font.BOLD,20));
			menu.setCursor(new Cursor(Cursor.HAND_CURSOR));
			
			item_help.setFont(new Font("Serif",Font.BOLD,20));
			item_help.setCursor(new Cursor(Cursor.HAND_CURSOR));
			item_help.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
                           try{
                           File path = new File("ManualDeUsuario.pdf");
                           //Runtime.getRuntime().exec("start ManualDeUsuario.pdf");
                           Desktop.getDesktop().open(path);
						   JOptionPane.showMessageDialog(null,"manual user");
                           }catch(Exception ep){}				

			}
		});
			close_u.setFont(new Font("Serif",Font.BOLD,20));
			close_u.setCursor(new Cursor(Cursor.HAND_CURSOR));
			close_u.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
                    int dialogWindow = JOptionPane.showConfirmDialog(null, "Seguro de cerrar el programa?"
	                                                   , "ADVICE", JOptionPane.YES_NO_OPTION);  
	                    if(dialogWindow == JOptionPane.YES_OPTION){
		                   try{
                            Controller.close(); 
					        //closeWindowsSession(frame);
							frame.dispose(); 
					        new LoginVista(); 
                           }catch(Exception ep){}
						}
						   				

			}
		});
			//lbl = new JLabel(""); 
		}
		
		public void addElement(){
			
			
			panel.add(vistaInsert, INSERT); 
            panel.add(vistaGuardar, UPDATE); 
		    panel.add(vistaMostrar, MOSTRAR);
			panel.add(vistaMostrarB, MOSTRARB);
			panel.add(vistaLog, LOGS);
			
			panelMenu.add(btnInsert); 
			panelMenu.add(btnModificar); 
			panelMenu.add(btnConsult); 
			//panelMenu.add(btnLog);
			panelMenu.add(labelIMG); 
			menu.add(item_help);
			menu.add(close_u);
			barr.add(menu);
			//barr.add(close_u);
			frame.setJMenuBar(barr);
			frame.addWindowListener(getWindowAdapter());
			frame.add(panel, BorderLayout.CENTER); 
			frame.add(panelMenu, BorderLayout.WEST); 
		}
		public static void getVisibleInsert(boolean status){
			btnInsert.setVisible(status);
		}
		public static void getVisibleConsult(boolean status){
			btnConsult.setVisible(status);
		}
		public static void getVisibleModificar(boolean status){
			btnModificar.setVisible(status);
		}
		
		public void setUpdateInfo(EspeciesAcuaticas especieAcuatica){
			vistaGuardar.setUpdateInfo(especieAcuatica);
		}
		
		public void imgFondo(){
		  
		}
		public void evenClick(){
			
			btnInsert.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ev){
					changePanel(INSERT);
				}
			});
			btnModificar.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ev){
					changePanel(UPDATE);
				}
			});
			btnConsult.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ev){
					changePanel(MOSTRAR);  
				}
			});
			btnLog.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ev){
					changePanel(LOGS);  
				}
			});
		}
		
		
		
		public void changePanel(String panelToChange){
			panelCard.show(panel, panelToChange);
		}
		
		private WindowAdapter getWindowAdapter() {
    return new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent we) {
        super.windowClosing(we);
        //JOptionPane.showMessageDialog(frame, "Cant Exit");
      }
      @Override
      public void windowIconified(WindowEvent we) {
        setState(JFrame.NORMAL);
        //frame.setSize(1000,300);
      }
    };
  }

   public void frame__windowStateChanged(WindowEvent e){
      // minimized
      if ((e.getNewState() & Frame.ICONIFIED) == Frame.ICONIFIED){
        
		
		frame.setSize(1020,540); 
		frame.setLocationRelativeTo(null);
      }
      // maximized
      else if ((e.getNewState() & Frame.MAXIMIZED_BOTH) == Frame.MAXIMIZED_BOTH){
        frame.setSize(1020,540); 
		frame.setLocationRelativeTo(null);
      }
   }
		
		public static void changePanelN(String panelToChange){
			panelCard.show(panel, panelToChange);
		}
		public void actualizarTabla(){
			vistaMostrar.actualizarTabla();
		}
		public void actualizarTablas(){
			vistaMostrarB.actualizarTablas();
		}
		public void cardMenu(JButton btnLog){
			btnLog.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ev){
					changePanel(LOGS);  
				}
			});
		}
		
		
}