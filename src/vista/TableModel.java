//version 1.1
//package view.framework;
package vista;

import java.lang.reflect.Field;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import dominio.controller.Controller; 

@SuppressWarnings("serial")
public class TableModel extends AbstractTableModel {
	private String[] columnNames = { "" };

    private Object[][] data = {
        {"No se encontraron registros"}};

    
    public TableModel( ){
    	
    }
    
    public TableModel(String[] columnNames,Object[][] data){
    	this.columnNames = columnNames;
    	this.data = data;
    }
    public int getColumnCount() {
      return columnNames.length;
      
    }

    public int getRowCount() {
      return data.length;
    }

    public String getColumnName(int col) {
      return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
      return data[row][col];
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	
    public Class getColumnClass(int c) {
      
	  Object o = getValueAt(0, c);
        if(o==null) return Object.class;
	  
	  return o.getClass();
    }      
//return (getValueAt(0, c) == null ? Object.class : getValue(0, c).getClass());
    //}
    
    public static <T> TableModel MakeTableModel(Class<T> modelClass,List<T>modelos){
    	String[] colname = makeColumnName(modelClass);
    	Object[][] data = makeData(modelos);
    	return new TableModel(colname,data);
    }
    
    private static <T> Object[][] makeData(List<T> modelos) {
    	int rows = modelos.size();
		int columns = modelos.get(0).getClass().getDeclaredFields().length;
    	Object[][] data = new Object[rows][columns];
		for(int i= 0; i< rows;i++){
			Field[] fields = modelos.get(0).getClass().getDeclaredFields();
			for(int j=0;j < columns;j++){
				try {
					fields[j].setAccessible(true);
					data[i][j] = fields[j].get(modelos.get(i));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return data;
	}

	private static <T> String[] makeColumnName(Class<T> modelClass){
    	Field[] atributos = modelClass.getDeclaredFields();
    	String [] columnNames = new String[atributos.length]; 
    	for(int i = 0;i<atributos.length;i++){
    		columnNames[i] = atributos[i].getName().substring(0,1).toUpperCase() + atributos[i].getName().substring(1);
    	}
    	return columnNames;
    }
}
