/**
   
   @author Kirlian Ortiz 
   @descripcion vistaUtilities 
   
**/ 
package vista; 

import javax.swing.*; 
import java.awt.*; 
import java.awt.event.*;
import javax.swing.table.*; 

import dominio.controller.Controller; 

public class VistaUtilities extends JFrame implements ActionListener{
  
  
  private static JFrame frame = null; 
 
  public VistaUtilities(){frame =  new JFrame();} 
  
  public void CambiarIcono(){
	  Image icono = Toolkit.getDefaultToolkit().getImage(getClass().getResource("img/img01.jpg"));
			
		    this.setIconImage(icono);
  } 
  
  public void cambiarImagen(String imagen){
	  //imagen = "imgPlanet.jpg"; 
	  
	  Image img = Toolkit.getDefaultToolkit().getImage(imagen); 
	  setIconImage(img); 
  }
  
  public void setColor(){
	  
		 JPanel pnlTop = new JPanel(new BorderLayout()) {
    protected void paintComponent(Graphics grphcs) {
        super.paintComponent(grphcs);
        Graphics2D g2d = (Graphics2D) grphcs;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        GradientPaint gp = new GradientPaint(0, 0,
                new java.awt.Color(67, 130, 179), 0, getHeight(),
                new java.awt.Color(222, 222, 222));
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, getWidth(), getHeight()); 
    }
 };
  
		
         pnlTop.setPreferredSize(new Dimension(200, 200));
        add(pnlTop, BorderLayout.SOUTH);
  }
  
  public void actionPerformed(ActionEvent ev){} 
  
  public static void closeWindows(Vista vista, JFrame frame){
   frame.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); 
   frame.addWindowListener(new CerrarVentana(vista));

   
}
 public static void closeWindowsSession(JFrame frame){
   frame.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); 
   frame.addWindowListener(new CerrarVentana());

   
}
} 

