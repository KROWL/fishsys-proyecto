/**
   
   @author Kirlian Ortiz 
   @descripcion VistaInsert  
   
**/ 
package vista;

import javax.swing.JFrame; 
import javax.swing.ImageIcon; 
import java.awt.Image;  
import java.awt.Dimension; 
import java.awt.Toolkit; 
import java.awt.event.ActionListener; 
import java.awt.event.ActionEvent;  
import javax.swing.JLabel; 
import javax.swing.JPanel; 
import java.awt.Color; 
import java.awt.Graphics; 
import javax.swing.JTable; 
import javax.swing.table.*;  
import java.awt.BorderLayout; 
import javax.swing.BorderFactory; 
import javax.swing.JPanel; 
import javax.swing.JTextField; 
import javax.swing.JButton; 
import java.awt.event.MouseAdapter; 
import java.awt.event.MouseEvent; 
import java.awt.Cursor; 
import java.awt.Font;
import java.awt.Graphics2D; 
import java.awt.GradientPaint; 
import java.awt.RenderingHints; 
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.UIManager;
import javax.swing.JOptionPane;
import dominio.controller.Controller; 
import dominio.EspeciesAcuaticas; 

public class ViewGuardar extends JPanel{
	
	private JLabel lblNombre = null; 
	private JLabel lblReino = null; 
	private JLabel lblFilo = null; 
	private JLabel lblClase = null; 
	private JLabel lblOrden = null; 
	private JLabel lblFamilia = null; 
	private JLabel lblGenero = null; 
	private JLabel lblTamano = null; 
	private JLabel lblNombreHabitat = null; 
	private JLabel lblTemperatura = null; 
	private JLabel lblOceano = null; 
	private JLabel lblProfundidad = null; 
	private JLabel lblMar = null; 
	private JLabel lblLatitud = null; 
	private JLabel lblLongitud = null; 
	private JLabel lblContinente = null; 
	private JLabel lblId = null; 
	
	private JTextField txtNombre = null; 
	private JTextField txtReino = null; 
	private JTextField txtFilo = null; 
	private JTextField txtClase = null; 
	private JTextField txtOrden = null; 
	private JTextField txtFamilia = null; 
	private JTextField txtGenero = null; 
	private JTextField txtTamano = null; 
	private JTextField txtNombreHabitat = null; 
	private JTextField txtTemperatura = null; 
	private JTextField txtOceano = null; 
	private JTextField txtProfundidad = null; 
	private JTextField txtMar = null; 
	private JTextField txtLatitud = null; 
	private JTextField txtLongitud = null; 
	private JTextField txtContinente = null; 
	private JTextField txtId = null; 
	
	private ImageIcon imgSave = null; 
	private ImageIcon imgClose = null; 
	
	private JButton btnInsert = null;
	private JButton btnClose = null;
	private Vista vista;
	Controller controller; 
	
	
	public ViewGuardar(Vista vista){
		
		
		iniElements(); 
		addVentana(); 
		addElement(); 
		cambiarColorFondo(); 
		//mostrarDatos(); 
		evenClick(); 
		JPanel panel = new JPanel(); 
		this.vista = vista;
		
	    // this.add(new JLabel("Label")); 
	} 
	
	public void cambiarColorFondo(){
		JPanel pnlTop = new JPanel(new BorderLayout()) {
    protected void paintComponent(Graphics grphcs) {
        super.paintComponent(grphcs);
        Graphics2D g2d = (Graphics2D) grphcs;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        GradientPaint gp = new GradientPaint(0, 0,
                new java.awt.Color(67, 130, 179), 0, getHeight(),
                new java.awt.Color(222, 222, 222));
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, getWidth(), getHeight()); 
    }
};
   
	}
	public void addVentana(){
		
		int Xx = 10;
		int Yy = 30;
		
		int Ww = 100; 
		int Hh = 25; 
		
		//setBounds Labels 
		lblId.setBounds     (Xx, Yy,       Ww, Hh);
		lblNombre.setBounds (Xx, Yy +  30, Ww, Hh);
		lblReino.setBounds  (Xx, Yy +  60, Ww, Hh);
		lblFilo.setBounds   (Xx, Yy +  90, Ww, Hh);
		lblClase.setBounds  (Xx, Yy + 120, Ww, Hh);
		lblOrden.setBounds  (Xx, Yy + 150, Ww, Hh);
		lblFamilia.setBounds(Xx, Yy + 180, Ww, Hh);
		lblGenero.setBounds (Xx, Yy + 210, Ww, Hh);
		lblTamano.setBounds (Xx, Yy + 240, Ww, Hh);
		
		//lbl.setBounds(200, 240, 80, 20);
		Xx = 250;
		Ww = 120; 
		lblNombreHabitat.setBounds (Xx, Yy,      Ww, Hh);
		lblTemperatura.setBounds   (Xx, Yy+ 30,  Ww, Hh);
		lblOceano.setBounds        (Xx, Yy+ 60,  Ww, Hh);
		lblProfundidad.setBounds   (Xx, Yy+ 90,  Ww, Hh);
		lblMar.setBounds           (Xx, Yy+ 120, Ww, Hh);
		lblLatitud.setBounds       (Xx, Yy+ 150, Ww, Hh);
		lblLongitud.setBounds      (Xx, Yy+ 180, Ww, Hh);
		lblContinente.setBounds    (Xx, Yy+ 210, Ww, Hh); 
		
		txtId = new JTextField(); 
		txtNombre = new JTextField();
		txtReino = new JTextField();
		txtFilo = new JTextField();
		txtClase = new JTextField();
		txtOrden = new JTextField();
		txtFamilia = new JTextField();
		txtGenero = new JTextField();
		txtTamano = new JTextField();
		txtNombreHabitat = new JTextField();
		txtTemperatura = new JTextField();
		txtOceano = new JTextField();
		txtProfundidad = new JTextField();
		txtMar = new JTextField();
		txtLatitud = new JTextField();
		txtLongitud = new JTextField();
		txtContinente = new JTextField(); 
		txtId.setEnabled(false); 
		
		// txt
		Xx = 100; 
		// Yy = 30; 
		Ww = 120; 
		txtId.setBounds     (Xx, Yy,        Ww, Hh); 
		txtNombre.setBounds (Xx, Yy +  30,  Ww, Hh); 
		txtReino.setBounds  (Xx, Yy +  60,  Ww, Hh); 
		txtFilo.setBounds   (Xx, Yy +  90,  Ww, Hh); 
		txtClase.setBounds  (Xx, Yy + 120,  Ww, Hh); 
		txtOrden.setBounds  (Xx, Yy + 150, Ww, Hh); 
		txtFamilia.setBounds(Xx, Yy + 180, Ww, Hh); 
		txtGenero.setBounds (Xx, Yy + 210, Ww, Hh); 
		txtTamano.setBounds (Xx, Yy + 240, Ww, Hh); 
		
		
		Xx = 370; 
		
		txtNombreHabitat.setBounds(Xx, Yy   ,    Ww, Hh); 
		txtTemperatura.setBounds  (Xx, Yy + 30,  Ww, Hh);  
		txtOceano.setBounds       (Xx, Yy + 60,  Ww, Hh); 
		txtProfundidad.setBounds  (Xx, Yy + 90,  Ww, Hh);  
		txtMar.setBounds          (Xx, Yy + 120, Ww, Hh); 
		txtLatitud.setBounds      (Xx, Yy + 150, Ww, Hh); 
		txtLongitud.setBounds     (Xx, Yy + 180, Ww, Hh); 
		txtContinente.setBounds   (Xx, Yy + 210, Ww, Hh); 
		
		
		//
		txtId.setToolTipText("numero de registro");
		txtNombre.setToolTipText("ingresar nombre");
		txtReino.setToolTipText("ingresar reino");
		txtFilo.setToolTipText("ingresar filo");
		txtClase.setToolTipText("ingresar clase");
		txtOrden.setToolTipText("ingresar orden");
		txtFamilia.setToolTipText("ingresar familia");
		txtGenero.setToolTipText("ingresar genero");
		txtTamano.setToolTipText("ingresar tamano");
		txtNombreHabitat.setToolTipText("ingresar nombre habitat");
		txtTemperatura.setToolTipText("ingresar temperatura");
		txtOceano.setToolTipText("ingresar oceano");
		txtProfundidad.setToolTipText("ingresar profundidad");
		txtMar.setToolTipText("ingresar mar");
		txtLatitud.setToolTipText("ingresar longitud");
		txtContinente.setToolTipText("ingresar continente");
		
		
		//setBounds btns 
		btnInsert.setBounds(100, 400, 150, 35); 
		 
		btnClose.setBounds(600, 20, 120, 25); 
		
		//styles btns 
		btnInsert.setFont(new Font("Serif",Font.BOLD,20));
        btnInsert.setCursor(new Cursor(Cursor.HAND_CURSOR));
		//btnInsert.setForeground(Color.WHITE);
		//btnInsert.setFocusPainted(false);
		btnInsert.setBorderPainted(false);
		btnInsert.addMouseListener(new java.awt.event.MouseAdapter() {
           public void mouseEntered(java.awt.event.MouseEvent evt) {
             btnInsert.setBackground(Color.decode("#4fc3f7"));
		   }
		   public void mouseExited(java.awt.event.MouseEvent evt) {
			 btnInsert.setBackground(UIManager.getColor("control"));
		   }
		});
		
		
		
		btnClose.setFont(new Font("Serif",Font.PLAIN,12));
		btnClose.setCursor(new Cursor(Cursor.HAND_CURSOR));
		//btnModificar.setForeground(Color.WHITE);
		btnClose.setFocusPainted(true);
		btnClose.setBorderPainted(false);
		btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
             public void mouseEntered(java.awt.event.MouseEvent evt) {
               btnClose.setBackground(new Color(0, 146, 255));
             }
       
             public void mouseExited(java.awt.event.MouseEvent evt) {
               btnClose.setBackground(UIManager.getColor("control"));
             }
        });
		
		//lbl = new JLabel(""); 
	}
	
	public void addElement(){
		
		this.setBorder(BorderFactory.createTitledBorder("Especies a modificar"));
		this.setLayout(null); 
		this.setBackground(new Color(67,130,179)); 
		
		this.add(lblId); 
		this.add(lblNombre); 
		this.add(lblReino); 
		this.add(lblFilo); 
		this.add(lblClase);
		this.add(lblOrden); 
		this.add(lblFamilia); 
		this.add(lblGenero); 
		this.add(lblTamano); 
		this.add(lblNombreHabitat); 
		this.add(lblTemperatura); 
		this.add(lblOceano); 
		this.add(lblProfundidad); 
		this.add(lblMar); 
		this.add(lblLatitud); 
		this.add(lblLongitud); 
		this.add(lblContinente); 
		
		this.add(txtNombre); 
		this.add(txtReino); 
		this.add(txtFilo); 
		this.add(txtClase); 
		this.add(txtOrden); 
		this.add(txtFamilia); 
		this.add(txtGenero); 
		this.add(txtTamano); 
		this.add(txtNombreHabitat); 
		this.add(txtTemperatura); 
		this.add(txtOceano); 
		this.add(txtProfundidad); 
		this.add(txtMar); 
		this.add(txtLatitud); 
		this.add(txtLongitud); 
		this.add(txtContinente); 
		
		this.add(txtId); 
		
		this.add(btnInsert); 
		 
		//this.add(btnClose); 
		
		
	}
	public void iniElements(){
		lblId = new JLabel("id"); 
		lblNombre = new JLabel("Nombre"); 
		lblReino = new JLabel("Reino"); 
		lblFilo = new JLabel("Filo"); 
		lblClase = new JLabel("Clase"); 
		lblOrden = new JLabel("Orden"); 
		lblFamilia = new JLabel("Familia"); 
		lblGenero = new JLabel("Genero"); 
		lblTamano = new JLabel("Tamano"); 
		
		lblNombreHabitat = new JLabel("Nombre del Habitat"); 
		lblTemperatura = new JLabel("Temperatura"); 
		lblOceano = new JLabel("Oceano"); 
		lblProfundidad = new JLabel("Profundidad"); 
		lblMar = new JLabel("Mar"); 
		lblLatitud = new JLabel("Latitud"); 
		lblLongitud = new JLabel("Longitud"); 
		lblContinente = new JLabel("Continente"); 
		
		controller = new Controller(); 
		
		//img btns 
		imgSave = new ImageIcon("img/save32x32_01.png"); 
		
		imgClose = new ImageIcon("img/close16x16.png"); 
		
		btnInsert = new JButton("Guardar", imgSave);
		btnClose = new JButton("cerrar session", imgClose);  
		
	}
	public void mostrarDatos(){
		 
		
		//txtId.setText(Integer.toString(valor)); 
	}
	public void evenClick(){
		
		btnInsert.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				
				String nombreComun   = ""; 
				String reino         = ""; 
				String filo          = ""; 
				String clase         = ""; 
				String orden         = ""; 
				String familia       = ""; 
				String genero        = ""; 
				String tamano        = ""; 
				String continente    = ""; 
				String mar           = ""; 
				String oceano        = ""; 
				String longitud      = ""; 
				String latitud       = ""; 
				String nombreHabitat = ""; 
				String profundidad   = ""; 
				String temperatura   = ""; 
				int id = 0; 
				
				nombreComun   = txtNombre.getText(); 
				reino         = txtReino.getText(); 
				filo          = txtFilo.getText(); 
				clase         = txtClase.getText(); 
				orden         = txtOrden.getText(); 
				familia       = txtFamilia.getText(); 
				genero        = txtGenero.getText(); 
				tamano        = txtTamano.getText(); 
				continente    = txtContinente.getText(); 
				mar           = txtMar.getText(); 
				oceano        = txtOceano.getText(); 
				longitud      = txtLongitud.getText(); 
				latitud       = txtLatitud.getText(); 
				nombreHabitat = txtNombreHabitat.getText(); 
				profundidad   = txtProfundidad.getText(); 
				temperatura   = txtTemperatura.getText(); 
				
				String idEspecie = txtId.getText();
				id = Integer.parseInt(idEspecie); 
				int val = controller.updateEspecies(nombreComun, reino, filo, clase, orden, familia, genero, tamano, continente, mar, oceano, longitud, latitud, nombreHabitat, profundidad, temperatura, id); 
		        //controller.insertPerson(nombreComun); 
				String nomb = controller.getNombre(); 
				System.out.println("Nombre: " + nomb); 
				//controller.setID(2); 
				int valor = controller.getID(); 
				System.out.println("id: " + valor); 
				System.out.println("id: " + val); 
				
				System.out.println("valor "); 
				//System.out.println("nombre " +nombre); 
				//JOptionPane.showMessageDialog(null, "Insert"); 
				
			}
		});
		btnClose.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				try{
				    //vista.changePanel(Vista.UPDATE);
					Controller.desconectar();
				   //controller.desconectar(); 
				   JOptionPane.showMessageDialog(null, "close session "); 
				   
				}catch(Exception ex){
					System.out.println("desconect!!"); 
				}
			}
		});
		
		/* btnModificar.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				JOptionPane.showMessageDialog(null, "modificar"); 
			}
		});
		btnConsult.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ev){
				JOptionPane.showMessageDialog(null, "serch"); 
			}
		}); */
	}
	public void setUpdateInfo(EspeciesAcuaticas especieAcuatica){
		txtId.setText(String.valueOf(especieAcuatica.getId()));
		txtNombre.setText       (especieAcuatica.getNombreComun()); 
		txtReino.setText        (especieAcuatica.getReino()); 
		txtFilo.setText         (especieAcuatica.getFilo()); 
		txtClase.setText        (especieAcuatica.getClase()); 
		txtOrden.setText        (especieAcuatica.getOrden()); 
		txtFamilia.setText      (especieAcuatica.getFamilia()); 
		txtGenero.setText       (especieAcuatica.getGenero()); 
		txtTamano.setText       (especieAcuatica.getTamano()); 
		txtContinente.setText   (especieAcuatica.getContinente()); 
		txtMar.setText          (especieAcuatica.getMar()); 
		txtOceano.setText       (especieAcuatica.getOceano()); 
		txtLongitud.setText     (especieAcuatica.getLongitud()); 
		txtLatitud.setText      (especieAcuatica.getLatitud()); 
		txtNombreHabitat.setText(especieAcuatica.getNombreHabitat()); 
		txtProfundidad.setText  (especieAcuatica.getProfundidad()); 
		txtTemperatura.setText  (especieAcuatica.getTemperatura()); 
	}
	public void setUpdateInfo(){
		//txtId.setText(String.valueOf(especieAcuatica.getId()));
		
	}
}