/**
   
   @author Kirlian Ortiz 
   @version 0 
   @descripcion Clase 
   
**/ 


package dominio.controller; 

import javax.swing.JOptionPane;
import java.sql.Connection; 
import java.sql.DriverManager; 
import java.sql.Statement; 
import java.sql.ResultSet; 
import java.sql.SQLException; 
import java.sql.ResultSetMetaData; 
import java.sql.DatabaseMetaData; 
import java.awt.*; 
import java.awt.event.*; 
import javax.swing.*; 

import dominio.Usuario; 
import DAL.InterfazDAL; 
import vista.SelectTables; 

public class Controller extends JFrame implements ActionListener{
   
   public boolean valida; 
   
   public static SelectTables sTab; 
   public Statement stmt; 
   public ResultSet rs; 
   public ResultSetMetaData rsm; 
   public DatabaseMetaData mtd; 
   public static String nombreUsuario; 
   
   
   public Controller(){
	   
   } 
   
   public Controller(SelectTables st) throws SQLException{
	   this.sTab = st; 
   }
   //validar al usuario 
   public boolean validarUsuario(String usuario, String contrasena){
       
	   
	   if(usuario == "" || contrasena == ""){
	     
		 JOptionPane.showMessageDialog(null, "Campos vacios ingrese usuario y contrasena "); 
		 
	  }else{ 
	  	 
		 try{
		    //enviar obj usuario pasar user a metodo enviar
			Usuario user = new Usuario(usuario, contrasena); 
			InterfazDAL DAL = new InterfazDAL(); 
			//validar conexion usando credenciales del usuario 
			valida = DAL.enviarValidacion(user); 
			nombreUsuario = user.getUsuario(); 
			
			if(valida){
			   
			   //CREAR VentanaPrincipal 
			   JOptionPane.showMessageDialog(null, "wellcome to FISHSYS " + nombreUsuario); 
			   SelectTables table = new SelectTables(); 
			   
			   DAL.executeGetTables(); 
			   
			   //DAL.consultarEspecies(); 
			   DAL.desconectar(); 
			   
			}else{
			    
				JOptionPane.showMessageDialog(null, "NO Conexion a la Base de Datos, USER NO VALIDO!!"); 
				//limpiar campos 
				usuario    = ""; 
				contrasena = ""; 
			}
			
		 }catch(SQLException e){
			 JOptionPane.showMessageDialog(null, "ERROR ", " ERROR ", JOptionPane.ERROR_MESSAGE); 
		 } 
		
      } 
	     return valida;
	   
   } 
   
   public static void close(){
	   
	   InterfazDAL.desconectar(); 
   } 
   
   public String getUsuario(){
	   
	   return nombreUsuario; 
   } 
   public void actionPerformed(ActionEvent e){
		
		 
	} 
   //public static SelectTables stable; 
   public void selectTabla(String nameTabla){
		
		//Vector<Object> filas; 
		
		/* for(int i=this.sTab.modelo.getRowCount(); i > 0; i--){
			this.sTab.modelo.removeRow(i-1); 
		} */
		
		
		 //conn.getConnectionDB(); 
		try{
		InterfazDAL conn = new InterfazDAL();
		    
		     
			stmt = conn.conexion().createStatement(); 
			rs = stmt.executeQuery("select * from FishSysDBA." + nameTabla); 

			rsm = rs.getMetaData(); 
			
			int col = rsm.getColumnCount(); 
			
			//nombre de las columnas 
			for(int i=1; i <= col; i++){
				this.sTab.modelo.addColumn(rsm.getColumnLabel(i)); 
			} 
			
			while(rs.next()){
				Object[] fila = new Object[col]; 
				for(int i=0; i < col; i++){
					fila[i] = rs.getObject(i+1); 
				}
				this.sTab.modelo.addRow(fila); 
			} 			
		}catch(Exception ex){
			
		}
	} 
   public void crearTablas(String tabla){
	     
	   
	   try{ 
	      
		  InterfazDAL dal = new InterfazDAL();
		  dal.selectTablas(tabla); 
		  
	   }catch(SQLException e){
		   
		   
	   }
	    
	   //return tabla; 
   }
   
   
}

