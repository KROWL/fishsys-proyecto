/**
   
   @author Kirlian Ortiz 
   @version 0 
   @descripcion Clase 
   
**/ 


package dominio.controller; 

import javax.swing.JOptionPane;
import java.sql.Connection; 
import java.sql.DriverManager; 
import java.sql.Statement; 
import java.sql.ResultSet; 
import java.sql.SQLException; 
import java.sql.ResultSetMetaData; 
import java.sql.DatabaseMetaData; 
import java.awt.*; 
import java.awt.event.*; 
import javax.swing.*; 

import java.lang.Integer; 
import java.util.List; 
import java.util.Vector; 

import dominio.Usuario; 
import DAL.InterfazDAL; 
import vista.Vista; 
import dominio.EspeciesAcuaticas; 
import vista.ITable; 
import vista.VistaMostrarB;
import vista.VistaMostrar; 
import vista.VistaLog;
import vista.TableModel; 
import dominio.Registro;
import vista.SelectTables; 

public class Controller extends JFrame implements ActionListener{
   
   public boolean valida; 
   
   public static SelectTables sTab; 
   public Statement stmt; 
   public ResultSet rs; 
   public ResultSetMetaData rsm; 
   public DatabaseMetaData mtd; 
   public static String nombreUsuario; 
   private int idEspecie = 0; 
   
   
   private EspeciesAcuaticas especies = new EspeciesAcuaticas();
   private Registro logs = new Registro();
   
   public Controller(){
	   
   } 
   
   public Controller(SelectTables st) throws SQLException{
	   this.sTab = st; 
   }
   //validar al usuario 
   public boolean validarUsuario(String usuario, String contrasena){
       
	   
	   if(usuario == "" || contrasena == ""){
	     
		 JOptionPane.showMessageDialog(null, "Campos vacios ingrese usuario y contrasena "); 
		 
	  }else{ 
	  	 
		 try{
		    //enviar obj usuario pasar user a metodo enviar
			Usuario user = new Usuario(usuario, contrasena); 
			InterfazDAL DAL = new InterfazDAL(); 
			//validar conexion usando credenciales del usuario 
			valida = DAL.enviarValidacion(user); 
			nombreUsuario = user.getUsuario(); 
			
			String role1=""; 
			
			if(valida){
			   
			   //CREAR VentanaPrincipal 
			   JOptionPane.showMessageDialog(null, "wellcome to FISHSYS " + nombreUsuario); 
			   //new Vista();  
			   cambioUsuario(nombreUsuario); 
			   //DAL.executeGetTables(); 
			   
			   //DAL.consultarEspecies(); 
			   DAL.desconectar(); 
			   
			}else{
			    
				JOptionPane.showMessageDialog(null, "NO Conexion a la Base de Datos, USER NO VALIDO!!"); 
				//limpiar campos 
				usuario    = ""; 
				contrasena = ""; 
			}
			
		 }catch(SQLException e){
			 JOptionPane.showMessageDialog(null, "ERROR ", " ERROR ", JOptionPane.ERROR_MESSAGE); 
		 } 
		
      } 
	     return valida;
	   
   } 
   
  public int insertarEspecie (String nombreComun,String reino,String filo,String clase,String orden,String familia,String genero, String tamano, String continente, String mar, String oceano, String longitud, String latitud, String nombreHabitat, String profundidad, String temperatura){
         int  val = 0; 
		 try{
		    //enviar obj usuario pasar user a metodo enviar
			
			especies.setNombreComun(nombreComun); 
			especies.setReino(reino); 
			especies.setFilo(filo); 
			especies.setClase(clase); 
			especies.setOrden(orden); 
			especies.setFamilia(familia); 
			especies.setGenero(genero); 
			especies.setTamano(tamano); 
			especies.setContinente(continente); 
			especies.setMar(mar); 
			especies.setOceano(oceano); 
			especies.setLatitud(latitud); 
			especies.setLongitud(longitud); 
			especies.setNombreHabitat(nombreHabitat); 
			especies.setProfundidad(profundidad); 
			especies.setTemperatura(temperatura); 
			
			//p.setReino(reino); 
			//p.setFilo(filo); 
			
			InterfazDAL dal = new InterfazDAL(); 
			//validar conexion usando credenciales del usuario 
			val = dal.insertarEspecie(especies); 
			 
			//usuarioMostrar = user.getUsuario(); 
			this.idEspecie = val; 
			if(val != 0){
			   
			   //CREAR VentanaPrincipal 
			   JOptionPane.showMessageDialog(null, "Se ha guardados los datos"); 
			  
			   //dao.desconectar(); 
			   
			}else{
			System.out.println("prueba@ "+val);
		}
			
			
		 }catch(SQLException e){
			 JOptionPane.showMessageDialog(null, "ERROR ", " ERROR ", JOptionPane.ERROR_MESSAGE); 
		 } 
		 
	     return val;
	   
   } 
   
   
  public int updateEspecies(String nombreComun, String reino, String filo, String clase, String orden, String familia, String genero, String tamano, String continente, String mar, String oceano, String latitud, String longitud, String nombreHabitat, String profundidad, String temperatura, int id){
	  int valor = 0; 
	  EspeciesAcuaticas especies = new EspeciesAcuaticas(); 
	  try{
			especies.setId(id); 
			especies.setNombreComun(nombreComun); 
			especies.setReino(reino); 
			especies.setFilo(filo); 
			especies.setClase(clase); 
			especies.setOrden(orden); 
			especies.setFamilia(familia); 
			especies.setGenero(genero); 
			especies.setTamano(tamano); 
			especies.setContinente(continente); 
			especies.setMar(mar); 
			especies.setOceano(oceano); 
			especies.setLatitud(latitud); 
			especies.setLongitud(longitud); 
			especies.setNombreHabitat(nombreHabitat); 
			especies.setProfundidad(profundidad); 
			especies.setTemperatura(temperatura); 
		
		
		InterfazDAL dao = new InterfazDAL();
		valor = dao.updateEspecies(especies);
		
		if(valor != 0){
			System.out.println("actualiza"); 
		}else{
			System.out.println("No actualizada ");
		}
	  }catch(Exception ex){
		  System.out.println("ERROR actualizando.. " + ex);
	  }
	 return valor;  
  }
   /* public void mostrarPersonas(){
	  try{
		  //person.setIdPersona(); 
	  new DAO().mostrarTabla(); 
		  
	  }catch(Exception ex){
		  
	  }
  }  */
   public void getEspecieAcuaticaTModel(ITable itable, String nombreComun){
		TableModel tmodel = new TableModel();
		int valor = 0;
		try{
			List<EspeciesAcuaticas> especiesAcuaticas = new InterfazDAL().getEspecieAcuatica(nombreComun);
			if(especiesAcuaticas.size()>0){				
				tmodel = TableModel.MakeTableModel(EspeciesAcuaticas.class, especiesAcuaticas);
				itable.loadTable(tmodel, new int []{0});
			}else if(valor != 0){
				System.out.println("bus "+nombreComun);
			}else{
				
			}
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("ERROR Especie Modelo" +e); 
		}
		itable.loadTable(tmodel,new int[]{});
  }
  public void getEspeciesAcuaticasTModel(ITable itable){
		TableModel tmodel = new TableModel();
		try{
			List<EspeciesAcuaticas> especiesAcuaticas = new InterfazDAL().getEspeciesAcuaticas();
			if(especiesAcuaticas.size()>0){				
				tmodel = TableModel.MakeTableModel(EspeciesAcuaticas.class, especiesAcuaticas);
				itable.loadTable(tmodel, new int []{0});
			}
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("ERROR Modelo Especies " +e); 
		}
		itable.loadTable(tmodel,new int[]{});
	  
  }
  
  //logs 
   public void getLogTModel(ITable itable, String nombreUsuario){
		TableModel tmodel = new TableModel();
		int valor = 0;
		try{
			List<Registro> logs = new InterfazDAL().getLog(nombreUsuario);
			if(logs.size()>0){				
				tmodel = TableModel.MakeTableModel(Registro.class, logs);
				itable.loadTable(tmodel, new int []{0});
				System.out.println("getTmodel "+nombreUsuario);
			}
				
			
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("ERROR logs Modelo" +e); 
		}
		itable.loadTable(tmodel,new int[]{});
  }
  
  public void getLogsTModel(ITable itable){
		TableModel tmodel = new TableModel();
		try{
			List<Registro> logs = new InterfazDAL().getLogs();
			
			if(logs.size()>0){
					
				tmodel = TableModel.MakeTableModel(Registro.class, logs);
				itable.loadTable(tmodel, new int []{0}); 
				//System.out.println("compu"); 
			}
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("ERROR logs Especies " + e); 
		}
		itable.loadTable(tmodel,new int[]{});
	  
  }
  
  public void setID(int id){
	this.idEspecie = id;   
  }
  
  public String getNombre(){
	  return especies.getNombreComun(); 
	  
  } 
  
  public int getID(){
	  return this.idEspecie; 
  }
public static Connection getConection(){
   return InterfazDAL.conn;
}
   public static void close(){
	   try{
		   
	      //InterfazDAL.desconectar(); 
	   }catch(Exception ex){
		   ex.printStackTrace();
	   }
   } 
   
   public static void desconectar(){
	   try{
		   
	      InterfazDAL.desconectar(); 
	   }catch(Exception ex){
		   
	   }
   } 
    
   
   public String getUsuario(){
	   
	   return nombreUsuario; 
   } 
   public void actionPerformed(ActionEvent e){
		
		 
	} 
   //public static SelectTables stable; 
   public void selectTabla(String nameTabla){
		
		//Vector<Object> filas; 
		
		/* for(int i=this.sTab.modelo.getRowCount(); i > 0; i--){
			this.sTab.modelo.removeRow(i-1); 
		} */
		
		
		 //conn.getConnectionDB(); 
		try{
		InterfazDAL conn = new InterfazDAL();
		    
		     
			stmt = conn.conexion().createStatement(); 
			rs = stmt.executeQuery("select * from FishSysDBA." + nameTabla); 

			rsm = rs.getMetaData(); 
			
			int col = rsm.getColumnCount(); 
			
			//nombre de las columnas 
			for(int i=1; i <= col; i++){
				this.sTab.modelo.addColumn(rsm.getColumnLabel(i)); 
			} 
			
			while(rs.next()){
				Object[] fila = new Object[col]; 
				for(int i=0; i < col; i++){
					fila[i] = rs.getObject(i+1); 
				}
				this.sTab.modelo.addRow(fila); 
			} 			
		}catch(Exception ex){
			
		}
	} 
	
   public String cambioUsuario(String usuario){
	   
	   
	   String userType = this.getUsuario(); 
	   
	   switch(usuario){
		   
		   case "FishSysDBA": 
		   
		   System.out.println(this.getUsuario()); 
		   new Vista(); 
		   
		   break; 
		   
		   case "Eduard": 
		   System.out.println(this.getUsuario()); 
		   try{ 
			 //InterfazDAL dal = new InterfazDAL(); 
			 InterfazDAL.setRole("biologo");   
			 
			 new Vista(); 
			 VistaMostrar.getVisibleLog(false); 
			   
		   }catch(Exception ex){
			   
		   }
		   
		   break; 
		   
		   case "carl": 
		   System.out.println(this.getUsuario()); 
		   try{ 
			 //InterfazDAL dal = new InterfazDAL(); 
			 InterfazDAL.setRole("BIOLOGO_READER");   
			 
			 Vista vistaMostrar = new Vista(); 
			 VistaMostrarB.getVisibleLog(false); 
			 Vista.getVisibleInsert(false);
			 Vista.getVisibleModificar(false);
			 Vista.getVisibleConsult(false);
			 VistaLog.getVisibleBack(false);
				
			 
			  vistaMostrar.changePanel(Vista.MOSTRARB);
		   }catch(Exception ex){
			   
		   }
		   
		   break;
		   
		   case "alfred": 
		   System.out.println(this.getUsuario()); 
		   try{ 
			 //InterfazDAL dal = new InterfazDAL(); 
			 InterfazDAL.setRole("biologo");   
			 
			 new Vista(); 
			 VistaMostrar.getVisibleLog(false); 
			   
		   }catch(Exception ex){
			   
		   }
		   break; 
		   
		   case "tesla": 
		   System.out.println(this.getUsuario()); 
		   try{ 
			 //InterfazDAL dal = new InterfazDAL(); 
			 InterfazDAL.setRole("biologo");   
			 
			 new Vista(); 
			 VistaMostrar.getVisibleLog(false); 
			   
		   }catch(Exception ex){
			   
		   }
		   break;
		   default: 
		   System.out.println("Usuario no valido"); 
	   }
	   return usuario; 
   }
   
   public void crearTablas(String tabla){
	     
	   
	   try{ 
	      
		  InterfazDAL dal = new InterfazDAL();
		  dal.selectTablas(tabla); 
		  
	   }catch(SQLException e){
		   
		   
	   }
	    
	   //return tabla; 
   }
   
   
}

