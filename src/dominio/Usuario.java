/**
   
   @author Kirlian Ortiz 
   @version 0 
   @descripcion Clase Usuario 
   
**/ 


package dominio; 

public class Usuario{
   
   String usuario; 
   String contrasena; 
   
   public Usuario(){
	   
	   this.usuario = ""; 
	   this.contrasena = ""; 
   } 
   
   public Usuario(String usuario, String contrasena){
      
	  this.usuario  = usuario; 
	  this.contrasena = contrasena; 
	  
   }
   
   public void setUsuario(String usuario){
      this.usuario = usuario; 
   }
   
   public void setPassword(String contrasena){
      this.contrasena = contrasena; 
   } 
   
   
   public String getUsuario(){
      return usuario; 
   } 
   
   public String getPassword(){
      return contrasena; 
   } 
   
   
}

