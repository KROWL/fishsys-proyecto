/**
   
   @author Kirlian Ortiz 
   @version 20.1 
   @description Clase Logs 
   
**/

package dominio; 


public class Registro{
  
  private String nombreUsuario = ""; 
  private int    IDlog         = 0;
  private String sessionUsuario = ""; 
  private String tablaNombre = ""; 
  private String fecha = ""; 
  public static Registro log;
  
  public Registro(){
	  nombreUsuario = "nom";
	  IDlog = 0;
	  sessionUsuario = "";
	  tablaNombre = "";
	  fecha = "";
  }
  
  public void setNombreUsuario(String nombreUsuario){
	  this.nombreUsuario = nombreUsuario; 
  }
  
  public void setId(int Idlog){
	  this.IDlog = Idlog;
  }
  public void setSessionUser(String sessionUsuario){
	  this.sessionUsuario = sessionUsuario; 
  }
  
  public void setTablaNombre(String tabla){
	  this.tablaNombre = tabla; 
  }
  
  public void setFecha(String fecha){
	  this.fecha = fecha; 
  }
  
  public String getNombreUsuario(){
	  return nombreUsuario; 
  }
  public int getIdlog(){
	  return IDlog;
  }
  public String getSessionUser(){
  	return sessionUsuario; 
  }
  public String getTablaNombre(){
	  return tablaNombre; 
  }
  public String getFecha(){
	  return fecha; 
  }
  
}