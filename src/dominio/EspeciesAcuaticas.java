/**
   
   @author Kirlian Ortiz 
   @descripcion vistaUtilities 
   
**/ 
package dominio; 

public class EspeciesAcuaticas{
   
   private int id = 0; 
   private String nombreComun   = ""; 
   private String reino         = ""; 
   private String filo          = ""; 
   private String clase         = ""; 
   private String orden         = ""; 
   private String familia       = ""; 
   private String genero        = ""; 
   private String tamano        = ""; 
   private String continente    = ""; 
   private String mar           = ""; 
   private String oceano        = ""; 
   private String longitud      = ""; 
   private String latitud       = ""; 
   private String nombreHabitat = ""; 
   private String profundidad   = ""; 
   private String temperatura   = ""; 
   
   
   public EspeciesAcuaticas(){
	   this.nombreComun = ""; 
	   this.reino = ""; 
	   this.filo = ""; 
	   this.clase = ""; 
	   this.orden = ""; 
	   this.familia = ""; 
	   this.genero = ""; 
	   this.tamano = ""; 
	   this.continente = ""; 
	   this.mar = ""; 
	   this.oceano = ""; 
	   this.longitud = ""; 
	   this.latitud = ""; 
	   this.nombreHabitat = ""; 
	   this.profundidad = ""; 
	   this.temperatura = ""; 
   }
   
   public void setNombreComun(String nombreComun){
	   this.nombreComun = nombreComun; 
   }
   public void setReino(String reino){
	   this.reino = reino; 
   }
   public void setFilo(String filo){
	   this.filo = filo; 
   }
   public void setClase(String clase){
	   this.clase = clase; 
   }
   public void setOrden(String orden){
	   this.orden = orden; 
   }
   public void setFamilia(String familia){
	   this.familia = familia; 
   }
   public void setGenero(String genero){
	   this.genero = genero; 
   }
   public void setTamano(String tamano){
	   this.tamano = tamano; 
   }
   public void setNombreHabitat(String nombreHabitat){
	   this.nombreHabitat = nombreHabitat; 
   }
   public void setTemperatura(String temperatura){
	   this.temperatura = temperatura; 
   }
   public void setOceano(String oceano){
	   this.oceano = oceano; 
   }
   public void setProfundidad(String profundidad){
	   this.profundidad = profundidad; 
   }
   public void setMar(String mar){
	   this.mar = mar; 
   }
   public void setLatitud(String latitud){
	   this.latitud = latitud; 
   }
   public void setLongitud(String longitud){
	   this.longitud = longitud; 
   }
   public void setContinente(String continente){
	   this.continente = continente; 
   }
   
   public void setId(int id){
	   this.id = id; 
   }
   
   public int getId(){
	   return id; 
   }
   public String getNombreComun(){
	   return nombreComun; 
   }
   public String getReino(){
	   return reino; 
   }
   public String getFilo(){
	   return filo; 
   }
   public String getClase(){
	   return clase; 
   }
   public String getOrden(){
	   return orden; 
   }
   public String getFamilia(){
	   return familia; 
   }
   public String getGenero(){
	   return genero; 
   }
   public String getTamano(){
	   return tamano; 
   }
   public String getNombreHabitat(){
	   return nombreHabitat; 
   }
   public String getTemperatura(){
	   return temperatura; 
   }
   public String getOceano(){
	   return oceano; 
   }
   public String getProfundidad(){
	   return profundidad; 
   }
   public String getMar(){
		return mar; 
   }
   public String getLatitud(){
	   return latitud; 
   }
   public String getLongitud(){
	   return longitud; 
   }
   public String getContinente(){
	   return continente; 
   }
   
}