import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

public class Reportes{

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args){		
        Map parametros = new HashMap();
        parametros.put("nombre ","Especies Acuaticas ");
        
       try {
            Planeta planeta = Planeta.crearR();
        	String Jasperfill = JasperCompileManager.compileReportToFile(".//Planeta.jrxml");
			String jasperprint = JasperFillManager.fillReportToFile(Jasperfill, parametros, new  JRBeanCollectionDataSource(planeta));
			JasperViewer.viewReport(jasperprint,false,false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
}
