/**
   
   @author Kirlian Ortiz 
   @version 0 
   @descripcion 
   
**/ 


package DAL; 

import java.sql.Connection; 
import java.sql.DriverManager; 
import java.sql.Statement; 
import java.sql.ResultSet; 
import java.sql.SQLException;  
import java.sql.PreparedStatement; 
import java.sql.ResultSetMetaData; 
import java.sql.DatabaseMetaData; 
import javax.swing.table.*; 
import javax.swing.JFrame;  
import javax.swing.JOptionPane; 
import javax.swing.JPanel; 
import java.awt.Dimension; 
import java.awt.BorderLayout; 
import javax.swing.JScrollPane; 
import javax.swing.JTable; 
import java.util.ArrayList; 
import java.lang.Integer; 
import java.util.List;
import java.util.Vector; 


import dominio.Usuario; 
import dominio.Registro;
import dominio.EspeciesAcuaticas; 


public class InterfazDAL{
   
   public static InterfazDAL single; 
   public static Connection conn = null; 
   public Statement stmt; 
   public ResultSet rs; 
   public ResultSetMetaData rsm; 
   public DatabaseMetaData mtd; 
   public static PreparedStatement pstmt = null;
   
   static boolean valida = false; 
   
   public InterfazDAL() throws SQLException{
      //enviarValidacion(Usuario usuario); 
	  conexion(); 
   } 
   //valido la conexion con el usuario  
   //public Connection getConnection(){
	 //  return conn; 
   //}
   public boolean enviarValidacion(Usuario usuario){
      
	  try{
	     
		 Class.forName("org.apache.derby.jdbc.ClientDriver"); 
		 String url = "jdbc:derby://localhost:1527/FishSysDB;user="+usuario.getUsuario()+";password="+usuario.getPassword();
		 //String url2 = "jdbc:derby://localhost:1527//FishSysDB;user=FishSysDBA;password=vostro1;bootPassword=connecti@4;"; 
		 
		 conn = DriverManager.getConnection(url+";bootPassword=connecti@4;");  
		 
		 valida = true; 
		 
		 if(conn != null){
			 JOptionPane.showMessageDialog(null, "Conectado!!"); 
		 }else{
			 JOptionPane.showMessageDialog(null, "NO Conectado!!");
		 } 
		 
		 
	  }catch(ClassNotFoundException e){
		 System.out.println("ERROR Conexion!!"+e); 
		 valida = false; 
	  }catch(SQLException ex){
		 System.out.println("ERROR de conexion"+ex); 
		 valida = false; 
	  }	
	  
	  return valida; 
   }
   
   public static Connection conexion(){
      return conn; 
   } 
   
   public void selectLogs(){
	   
   }
   public static String setRole(String role){ 
   
		   //metodos estaticos variables estaticas 
	   try{
		   if(conn !=null){
			   
			   pstmt = conn.prepareStatement("set role ?"); 
			   pstmt.setString(1, role); 
			   pstmt.executeUpdate(); 
			    
		   }else{
			   System.out.println("NO role set");
		   }
		   
	   }catch(Exception ex){
		   System.out.println("ERROR Role" +ex); 
	   }
	   return role; 
   }
   public int insertarEspecie(EspeciesAcuaticas especies){
	   int identity = 0; 
	   try{
		   valida = true; 
		    if(conn != null){
		     
			//stmt = conn.getConnectionDB().createStatement(); 
			//stmt.execute("INSERT INTO PLANETAS(nombrePlaneta,diametroPlaneta,masaPlaneta,radioPlaneta,periodoPlaneta) VALUES (?,?,?,?,?)"); 
			pstmt = conn.prepareStatement("INSERT INTO FishSysDBA.EspeciesAcuaticas(nombreComun) VALUES (?)",Statement.RETURN_GENERATED_KEYS); 
			pstmt.setString(1,especies.getNombreComun()); 
			pstmt.executeUpdate(); 
			pstmt.close(); 
			
			pstmt = conn.prepareStatement("select identity_val_local() as identity_val from sysibm.sysdummy1");
			ResultSet rs = pstmt.executeQuery(); 
			
			if(rs.next()){
				identity = rs.getInt(1); 
			}
			
			rs.close(); 
			pstmt.close(); 
			
			pstmt = conn.prepareStatement("INSERT INTO FishSysDBA.Taxonomias"
			                                                +"(idEspecie,reino,filo,clase,orden,familia,genero,tamano)"
															+" VALUES (?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			
			
			pstmt.setInt(1,identity); 
			pstmt.setString(2,especies.getReino()); 
			pstmt.setString(3,especies.getFilo());
			pstmt.setString(4,especies.getClase()); 
			pstmt.setString(5,especies.getOrden()); 
			pstmt.setString(6,especies.getFamilia());
			pstmt.setString(7,especies.getGenero());
			pstmt.setString(8,especies.getTamano());
			
			pstmt.executeUpdate(); 
			pstmt.close(); 
			
			pstmt = conn.prepareStatement("select identity_val_local() as identity_val from sysibm.sysdummy1");
			rs = pstmt.executeQuery(); 
			
			if(rs.next()){
				identity = rs.getInt(1); 
			}
			
			rs.close(); 
			pstmt.close(); 
			
			pstmt = conn.prepareStatement("INSERT INTO FishSysDBA.ZonasGeos"
			                                                +"(continente,tipoMasaAcuatica,nombreMasaAcuatica)"
															+" VALUES (?,?,?)",Statement.RETURN_GENERATED_KEYS);
			
			
			pstmt.setString(1,especies.getContinente()); 
			pstmt.setString(2,especies.getMar()); 
			pstmt.setString(3,especies.getOceano()); 
			
			pstmt.executeUpdate(); 
			pstmt.close();
			 
			pstmt = conn.prepareStatement("select identity_val_local() as identity_val from sysibm.sysdummy1");
			rs = pstmt.executeQuery(); 
			
			if(rs.next()){
				identity = rs.getInt(1); 
			}
			
			rs.close(); 
			pstmt.close(); 
			
			pstmt = conn.prepareStatement("INSERT INTO FishSysDBA.CoordenadasGeograficas"
			                                                +"(idZonaGeo, longitud,latitud)"
															+" VALUES (?,?,?)",Statement.RETURN_GENERATED_KEYS);
			
			
			pstmt.setInt(1,identity); 
			pstmt.setString(2,especies.getLongitud());
			pstmt.setString(3,especies.getLatitud()); 
			pstmt.executeUpdate(); 
			pstmt.close(); 
			
			pstmt = conn.prepareStatement("select identity_val_local() as identity_val from sysibm.sysdummy1");
			rs = pstmt.executeQuery(); 
			
			if(rs.next()){
				identity = rs.getInt(1); 
			}
			
			rs.close(); 
			pstmt.close(); 
			
			pstmt = conn.prepareStatement("INSERT INTO FishSysDBA.Habitats"
			                                                +"(idZonaGeo, nombre,profundidad,temperatura)"
															+" VALUES (?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			
			
			pstmt.setInt(1,identity); 
			pstmt.setString(2,especies.getNombreHabitat());
			pstmt.setString(3,especies.getProfundidad()); 
			pstmt.setString(4,especies.getTemperatura()); 
			pstmt.executeUpdate(); 
			pstmt.close(); 
			pstmt = conn.prepareStatement("select identity_val_local() as identity_val from sysibm.sysdummy1");
			rs = pstmt.executeQuery(); 
			
			if(rs.next()){
				identity = rs.getInt(1); 
			}
			
			rs.close(); 
			pstmt.close(); 
			
			pstmt = conexion().prepareStatement("INSERT INTO FishSysDBA.EspeciesPorHabitats"
			                                                +"(idHabitat,idEspecie)"
															+" VALUES (?,?)",Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1,identity); 
			pstmt.setInt(2,identity); 
			pstmt.executeUpdate(); 
			pstmt.close(); 
			
			//ResultSet tableKeys = pstmt.getGeneratedKeys(); 
			//tableKeys.next();
            //int autoGeneratedID = tableKeys.getInt(1);
			 
			}else{
			System.out.println("datos NO insert"); 
				
			}
		}catch(Exception ex){
			valida = false; 
			System.out.println("ERROR, INSERT"); 
		} 
		//single.desconectar(); 
		return identity; 
   }
   
  
  public int updateEspecies(EspeciesAcuaticas especie){
    int columnas = 0;
    try {
		pstmt = conn.prepareStatement("UPDATE FishSysDBA.EspeciesAcuaticas SET nombreComun = ? WHERE idEspecie = ?");
		pstmt.setString(1,especie.getNombreComun()); 
		pstmt.setInt(2, especie.getId()); 
		columnas = pstmt.executeUpdate(); 
		pstmt.close(); 
		
		pstmt = conn.prepareStatement("UPDATE FishSysDBA.Taxonomias SET reino = ?, filo = ?, clase = ?, orden = ?,familia = ?, genero = ?, tamano = ? WHERE idTaxonomia = ?");

		pstmt.setString(1,especie.getReino()); 
		pstmt.setString(2,especie.getFilo());
		pstmt.setString(3,especie.getClase()); 
		pstmt.setString(4,especie.getOrden()); 
		pstmt.setString(5,especie.getFamilia());
		pstmt.setString(6,especie.getGenero());
		pstmt.setString(7,especie.getTamano()); 
		pstmt.setInt(8, especie.getId()); 
		columnas = pstmt.executeUpdate(); 
		pstmt.close(); 
		
		pstmt = conn.prepareStatement("UPDATE FishSysDBA.ZonasGeos set "
														+"continente = ?, tipoMasaAcuatica = ?, nombreMasaAcuatica = ? where idZonaGeo = ?"); 

		pstmt.setString(1,especie.getContinente()); 
		pstmt.setString(2,especie.getMar()); 
		pstmt.setString(3,especie.getOceano()); 
		pstmt.setInt(4,especie.getId()); 
		columnas = pstmt.executeUpdate(); 
		pstmt.close(); 
		
		
		pstmt = conn.prepareStatement("UPDATE FishSysDBA.CoordenadasGeograficas set "+" longitud = ?, latitud = ? WHERE idCoord_Geo = ?"); 
		
		pstmt.setString(1,especie.getLongitud());
		pstmt.setString(2,especie.getLatitud()); 
		pstmt.setInt(3,especie.getId()); 
		columnas = pstmt.executeUpdate(); 
		pstmt.close(); 
		
		pstmt = conn.prepareStatement("UPDATE FishSysDBA.Habitats SET "+" nombre = ?, profundidad = ?, temperatura = ? WHERE idHabitat = ?"); 
		
		pstmt.setString(1,especie.getNombreHabitat());
		pstmt.setString(2,especie.getProfundidad()); 
		pstmt.setString(3,especie.getTemperatura()); 
		pstmt.setInt(4,especie.getId()); 
		columnas = pstmt.executeUpdate(); 
		pstmt.close(); 
		
		//columnas = pstmt.executeUpdate(); 
		
    }catch (SQLException e) {
      //e.printStackTrace();
	  System.out.println("ERORR ACTUALIZAR DATOS: " + e);
    }catch (ArrayIndexOutOfBoundsException ex) {
      //e.printStackTrace();
	  System.out.println("ERORR ARRAY: " + ex);
    }

    return columnas;
  }
  
     public List<EspeciesAcuaticas> getEspecieAcuatica(String buscarNombreComun){
		List<EspeciesAcuaticas> especiesAcuaticas = new Vector<EspeciesAcuaticas>();
	   
	   try{  
	        String query = "select es.idEspecie, es.nombreComun, t.clase, t.familia, t.filo, t.genero, t.orden, t.reino, t.tamano,"
			                               +"zona.continente,zona.nombreMasaAcuatica, zona.tipoMasaAcuatica, coo.latitud, coo.longitud,"
			                               +"h.nombre, h.profundidad, h.temperatura from FishSysDBA.EspeciesPorHabitats as esp inner join "
			                               +"FishSysDBA.Taxonomias as t on esp.idEspecie = t.idEspecie inner join "
			                               +"FishSysDBA.EspeciesAcuaticas as es on es.idEspecie = t.idEspecie inner join "
			                               +"FishSysDBA.Habitats h on h.idHabitat = esp.idHabitat inner join "
			                               +"FishSysDBA.ZonasGeos zona on zona.idZonaGeo = h.idZonaGeo inner join "
			                               +"FishSysDBA.CoordenadasGeograficas as coo on coo.idZonaGeo = zona.idZonaGeo "
			                               + " where LOWER(es.nombreComun) like LOWER(?)"; 
			//String sql = String.format(query, "SUBSTR(nombreComun,27,16)");
			 
			//buscarNombreComun.replace("!", "!!").replace("%", "!%").replace("_", "!_").replace("[", "![");
			pstmt = conn.prepareStatement(query); 
			//buscarNombreComun = buscarNombreComun.toUpperCase();
			pstmt.setString(1,"%" + buscarNombreComun +"%");
			rs = pstmt.executeQuery(); 
			
			
			
			while(rs.next()) {
				EspeciesAcuaticas especieAcuatica = new EspeciesAcuaticas(); 
				especieAcuatica.setId(Integer.parseInt(rs.getString("idEspecie"))); 
				especieAcuatica.setNombreComun(rs.getString("nombreComun")); 
				especieAcuatica.setReino(rs.getString("reino")); 
				especieAcuatica.setFilo(rs.getString("filo")); 
				especieAcuatica.setClase(rs.getString("clase")); 
				especieAcuatica.setOrden(rs.getString("orden")); 
				especieAcuatica.setFamilia(rs.getString("familia")); 
				especieAcuatica.setGenero(rs.getString("genero")); 
				especieAcuatica.setTamano(rs.getString("tamano")); 
				especieAcuatica.setContinente(rs.getString("continente")); 
				especieAcuatica.setMar(rs.getString("tipoMasaAcuatica")); 
				especieAcuatica.setOceano(rs.getString("nombreMasaAcuatica")); 
				especieAcuatica.setLongitud(rs.getString("longitud")); 
				especieAcuatica.setLatitud(rs.getString("latitud")); 
				especieAcuatica.setNombreHabitat(rs.getString("nombre")); 
				especieAcuatica.setProfundidad(rs.getString("profundidad")); 
				especieAcuatica.setTemperatura(rs.getString("temperatura"));
				especiesAcuaticas.add(especieAcuatica);
			}
			rs.close(); 
			pstmt.close(); 
	   }catch(Exception ex){
		   ex.printStackTrace();
		   System.out.println("ERROR Mostrar tabla" +ex); 
	   }
	   return especiesAcuaticas; 
   }
   
      public List<EspeciesAcuaticas> getEspeciesAcuaticas( ){
		List<EspeciesAcuaticas> especiesAcuaticas = new Vector<EspeciesAcuaticas>();
	   
	   try{  
			pstmt = conn.prepareStatement("select es.idEspecie, es.nombreComun, t.clase, t.familia, t.filo, t.genero, t.orden, t.reino, t.tamano,"
			                               +"zona.continente,zona.nombreMasaAcuatica, zona.tipoMasaAcuatica, coo.latitud, coo.longitud,"
			                               +"h.nombre, h.profundidad, h.temperatura from FishSysDBA.EspeciesPorHabitats as esp inner join "
			                               +"FishSysDBA.Taxonomias as t on esp.idEspecie = t.idEspecie inner join "
			                               +"FishSysDBA.EspeciesAcuaticas as es on es.idEspecie = t.idEspecie inner join "
			                               +"FishSysDBA.Habitats h on h.idHabitat = esp.idHabitat inner join "
			                               +"FishSysDBA.ZonasGeos zona on zona.idZonaGeo = h.idZonaGeo inner join "
			                               +"FishSysDBA.CoordenadasGeograficas as coo on coo.idZonaGeo = zona.idZonaGeo ");  
			rs = pstmt.executeQuery(); 
			
			
			
			while(rs.next()) {
				EspeciesAcuaticas especieAcuatica = new EspeciesAcuaticas(); 
				especieAcuatica.setId(Integer.parseInt(rs.getString("idEspecie"))); 
				especieAcuatica.setNombreComun(rs.getString("nombreComun")); 
				especieAcuatica.setReino(rs.getString("reino")); 
				especieAcuatica.setFilo(rs.getString("filo")); 
				especieAcuatica.setClase(rs.getString("clase")); 
				especieAcuatica.setOrden(rs.getString("orden")); 
				especieAcuatica.setFamilia(rs.getString("familia")); 
				especieAcuatica.setGenero(rs.getString("genero")); 
				especieAcuatica.setTamano(rs.getString("tamano")); 
				especieAcuatica.setContinente(rs.getString("continente")); 
				especieAcuatica.setMar(rs.getString("tipoMasaAcuatica")); 
				especieAcuatica.setOceano(rs.getString("nombreMasaAcuatica")); 
				especieAcuatica.setLongitud(rs.getString("longitud")); 
				especieAcuatica.setLatitud(rs.getString("latitud")); 
				especieAcuatica.setNombreHabitat(rs.getString("nombre")); 
				especieAcuatica.setProfundidad(rs.getString("profundidad")); 
				especieAcuatica.setTemperatura(rs.getString("temperatura"));
				especiesAcuaticas.add(especieAcuatica);
			}
			rs.close(); 
			pstmt.close(); 
			
	   }catch(Exception ex){
		   ex.printStackTrace();
	   }
	   return especiesAcuaticas; 
   }
   
   //logs tabla 
   
    public List<Registro> getLog(String buscarNombre){
		List<Registro> log = new Vector<Registro>();
	   
	   try{  
	        String query = "select usuario, session_usuario, nombre_tabla, fecha"
			                               +" from  fishsysdba.logs where LOWER(session_usuario) like LOWER(?) "; 
			
			pstmt = conn.prepareStatement(query); 
			//buscarNombreComun = buscarNombreComun.toUpperCase();
			pstmt.setString(1,"%" + buscarNombre +"%");
			rs = pstmt.executeQuery(); 
			System.out.println("getL "+buscarNombre);
			
			
			while(rs.next()) {
				Registro logs = new Registro(); 
				//logs.setId(Integer.parseInt(rs.getString("idlog"))); 
				logs.setNombreUsuario(rs.getString("usuario")); 
				logs.setSessionUser(rs.getString("session_usuario"));
				logs.setTablaNombre(rs.getString("nombre_tabla"));
				logs.setFecha(rs.getString("fecha"));
				log.add(logs);
			}
			rs.close(); 
			pstmt.close(); 
	   }catch(Exception ex){
		   ex.printStackTrace();
		   System.out.println("ERROR Mostrar tabla" +ex); 
	   }
	   return log; 
   }
   
      public List<Registro> getLogs( ){
		List<Registro> log = new Vector<Registro>();
	   
	   try{  
			 pstmt = conn.prepareStatement("select usuario, session_usuario, nombre_tabla, fecha from FishSysDBA.logs ORDER BY fecha DESC");  
			rs = pstmt.executeQuery();  
			
			
			//log.setId((int)tabla.getModel().getValueAt(tabla.getSelectedRow(),0)); 
								/* log.setNombreUsuario  ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),0));
								log.setSessionUser        ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),1));
								log.setTablaNombre         ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),2));
								log.setFecha        ((String)tabla.getModel().getValueAt(tabla.getSelectedRow(),3));
								  */
								//vista.setUpdateInfo(log);
								//vista.changePanel(Vista.LOGS);
								
								
			while(rs.next()) {
				Registro logs = new Registro(); 
				//logs.setId(Integer.parseInt(rs.getString("idcompania"))); 
				logs.setNombreUsuario(rs.getString("usuario"));
				logs.setSessionUser(rs.getString("session_usuario"));
				logs.setTablaNombre(rs.getString("nombre_tabla"));
				logs.setFecha(rs.getString("fecha"));
				
				log.add(logs);
			} 
			rs.close(); 
			pstmt.close(); 
			
	   }catch(Exception ex){
		   ex.printStackTrace();
	   }
	   return log; 
   }
   
   
   public String[] executeGetTables() {
      
	  ArrayList<String> tablas = new ArrayList<String>(); 
	  String tmp= ""; 
	  
   
   try {  
      DatabaseMetaData dbmd = conexion().getMetaData();  
      ResultSet rs = dbmd.getTables(null, null, "%", new String[]{"TABLE"});  
      stmt = conexion().createStatement(); 
	  //ResultSetMetaData rsmd = rs.getMetaData();  
  
      // Display the result set data.  
      //int cols = rsmd.getColumnCount();  
      while(rs.next()) {
		  
		  try{
			  
			  tmp = rs.getString("TABLE_NAME"); 
			  ResultSet srs = stmt.executeQuery("select * from FishSysDBA." +tmp); 
			  tablas.add(tmp); 
			  
		  }catch(SQLException e){
			  
		  }
		  
		  String schema = rs.getString(2); 
	      String tablaNombre = rs.getString(3); 
	      System.out.println("SCHEMA: " +schema+ " Tabla1: " +tablaNombre);
      }  
	  
	  
      rs.close();  
	  
	  
   }   
  
   catch (Exception e) {  
      e.printStackTrace();  
   }  
   
   String[] tablaArray = new String[tablas.size()]; 
   tablaArray = tablas.toArray(tablaArray); 
   
   return tablaArray; 
}
   
   public int getFilas(String nombTabla){
	   
	   int cant = 0 ; 
	   
	   try{
		  
		  stmt = conexion().createStatement(); 
		  rs   = stmt.executeQuery("select * from FishSysDBA." + nombTabla); 
		  
		  while(rs.next()){
			  
			  cant++; 
			  
		  }
		  
	   }catch(SQLException e){
		   
		   System.out.println("ERROR: " + e.getMessage()); 
		   
	   } 
	   
	   return cant; 
   }
   
   public String[][] selectTablas(String tabla){
	   
	   String[][] fila = null; 
	   int cont = 0; 
	   
	   try{
			
			
			stmt = conexion().createStatement(); 
			rs   = stmt.executeQuery("select * from FishSysDBA." +tabla);	
			rsm  = rs.getMetaData(); 
			//contar col  
			int col = rsm.getColumnCount(); 
			//obtener el numero de filas 
			int rows = getFilas(tabla); 
			fila = new String[rows][col]; 
			
			while(rs.next()){
				 
				for(int i=0; i < col; i++){
					fila[cont][i] = rs.getString(i+1); 
				} 
				cont++; 
			} 
			
			String nameCol = rsm.getColumnName(1); 
			
			System.out.println("Nombre Col "+ nameCol); 
			
			
		}catch(SQLException ex){
			
			System.out.println("ERROR "+ex); 
		} 
	   
		
		return fila; 
   }
  
   
   public static boolean desconectar(){
	   
	  
	   
		boolean  valor = false;
      if(single!=null){
		 
	     try{
			 
		    conn.close(); 
			 valor = true;
		 }catch(SQLException e){
		    e.printStackTrace();
		 }
	  } 
	  return valor; 
   } 
   
   
   
}
